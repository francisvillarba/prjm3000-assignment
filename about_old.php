<?php
// set the expiration date to one hour ago
session_start();
//setcookie("PHPSESSID", "1234567890", time() + 1440, "/", "iforgetpdm.tk", true );
?>

<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

<head>
    <!-- Information for the Site -->
    <meta charset="UTF-8">
    <title>Suggestion Box</title>

    <meta name="keywords" content="iForget, PDM, PRJM3000, Curtin University, Project, Suggestions, Suggestion-Box">
    <meta name="description"
          content="Suggestion-Box - A project providing students and educators a new way to share and communicate ideas, suggestions and concepts">

    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="css/main.css"/>
    <link type="text/css" rel="stylesheet" href="css/loading.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" href="apple-touch-icon.png">
    <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<!-- Navigation Bar / Header -->
<nav class="top-page teal" role="navigation" id="top">
    <div class="nav-wrapper container">
        <!-- What the desktops and HiDPI large screens will see -->
        <a id="logo-container" href="#" class="brand-logo">iForget</a>
        <ul class="modal-trigger-TOS right waves-effect hide-on-med-and-down" href="#registerTOS" onclick="updateTOSText()"> <!-- Register Button -->
            <li><a href="#">Register</a></li>
        </ul>
        <ul class="right waves-effect waves-light hide-on-med-and-down modal-trigger" href="#loginDialog"> <!-- Login Button -->
            <li><a>Login</a></li>
        </ul>
        <!-- What mobiles will see -->
        <!-- TODO -- What will the mobile phone users see? -- Most like another page just for login-->
        <ul id="slide-out" class="side-nav teal">
            <li><a href="#mobileTOS1" class="hoverBlack waves-effect modal-trigger-mobile-TOS" onclick="updateMobileTOSText()"><i class="tiny left white-text material-icons modal-trigger-mobile-TOS" href="mobileTOS1" onclick="updateMobileTOSText()">input</i>Login</a></li>
            <li><a href="#" class="hoverBlack waves-effect"><i class="tiny left white-text material-icons">perm_identity</i>Register</a></li>
        </ul>
        <a href="#" data-activates="slide-out" class="button-collapse"><i class="material-icons white-text">menu</i></a>
    </div>
</nav>

<!-- Initial Slice -->
<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>
            <div class="row center">
                <br><br>
                <h1 class="header center white-text text-lighten-1"><strong>Suggestion Box</strong></h1>
                <h5 class="header col s12 white-text text-shadow">A modern take on student to educator communications</h5>
            </div>
            <div class="row center">
                <a href="#loginDialog" id="desktopStarted" class="btn-large waves-effect waves-light teal lighten-1 modal-trigger hide-on-med-and-down">Get started</a>
            </div>
            <br><br>
        </div>
    </div>
    <div class="parallax"><img src="media/img/Earth-and-Moon.jpg" alt="Earth and Moon Parallax"></div>
</div>

<!-- Second Slice -->
<div class="container">
    <div class="section">
        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">flash_on</i></h2>
                    <h5 class="center">Bring on the ideas!</h5>
                    <p class="light center">Our aim is to give students a platform and place to voice their ideas quickly, easily and effortlessly through iForget's innovative Suggestion-Box online platform. iForget strives to empower students with a clear voice and gives educators the chance to hear it all!</p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">group</i></h2>
                    <h5 class="center">User Experience Focused</h5>
                    <p class="light center">By utilizing elements and principles of modern design, we were able to create a framework that incorporates components and animations that provide clear feedback to users. Through the use a single underlying responsive system across all platforms this allows for a unified and pleasing experience.</p>
                </div>
            </div>

            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">settings</i></h2>
                    <h5 class="center">Easy to work with</h5>
                    <p class="light center">We have provided detailed documentation as well as examples to help new users get started. We are also always open to feedback and can answer any questions a user may have about the Suggestion-Box platform.</p>
                </div>
            </div>
        </div>

    </div>
</div>

<!-- Third Slice -->
<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light text-shadow"><b>The sky's the limit</b></h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="media/img/Moon.jpg" alt="Earth and Moon Parallax"></div>
</div>

<!-- Fourth Slice -->
<div class="container">
    <div class="section">
        <div class="row">
            <div class="col s12 center">
                <h3><i class="mdi-content-send green-text"></i></h3>
                <h4>Never Give up - Never Settle</h4>
                <p class="left-align light center">Do it! Just do it! <b>Don't let your dreams be dreams.</b> Yesterday, you said tomorrow. So just do it! <br> <b>Make your dreams come true!</b> Just do it! Some people dream of success, while you're gonna wake up and work hard at it! <br> <b>Nothing is impossible!</b> You should get to the point where anyone else would quit, and <b>you're not gonna stop there!</b> <br> No, what are you waiting for? Do it! Just do it! Yes you can! Just do it.
                    If you're tired of starting over, <b>stop giving up!</b></p>
            </div>
        </div>
    </div>
</div>

<!-- Fifth Slice -->
<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light black-text text-shadow-white">Scroll down more to find out more about iForget</h5>
                <a class="btn-large btn-large waves-effect waves-light teal scroll-top" href="#top">Back to top</a>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="media/img/Yosemite-3.jpg" alt="Sky and Ground Parallax"></div>
</div>

<!-- Footer -->
<footer class="page-footer teal">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">iForget PDM Group</h5>
                <p class="grey-text text-lighten-4">We are a small, close-nit group of friends that banded together with the goal to change the world through new ideas, experiences and innovation.</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">More iForget</h5>
                <ul>
                    <!-- TODO -- Fill these links in -->
                    <li><a class="grey-text text-lighten-3" href="#!">The Team</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">Our Mission</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">References</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container center">
            <p class="white-text">&copy; 2016 iForget PDM Group & Curtin University of Western Australia</p>
        </div>
    </div>
</footer>

<!-- All the dialog boxes go here -->

<!-- Login Dialog -->
<div id="loginDialog" class="modal modal-fixed-footer login">
    <div class="modal-content">
        <h4 class="center">Login</h4>
        <p class="center">Welcome back to iForget's Suggestion-Box</p>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">email</i>
                <input id="username" type="email" class="validate">
                <label for="username" data-error="Invalid input" data-success="">Email Address</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">vpn_key</i>
                <input id="password" type="password" class="validate">
                <label for="password">Password</label>
            </div>
        </div>
        <div class="row">
            <p class="center">Don't have an account? <a href="#registerTOS" class="modal-close modal-trigger-TOS" onclick="openTosFromLogin()">Click here to register!</a></p>
        </div>
    </div>
    <div class="modal-footer">
        <!-- TODO -- Actually have the login button doing something -->
        <a href="#!" class="modal-action waves-effect waves-light teal btn-flat white-text" onclick="loginMain()">Login</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="buttonModalCancelLogin()">Cancel </a>
    </div>
</div>

<!-- Before you register -->
<div id="registerTOS" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 class="center">Before you register</h4>
        <p class="center">By agreeing to register you agree to the following TOS, Privacy Policy and Security Policy</p>
        <ul class="collapsible z-depth-0" data-collapsible="expandable">
            <li>
                <div class="collapsible-header"><i class="material-icons">subject</i>Terms and Conditions</div>
                <div class="collapsible-body"><p id="tos"></p></div>
            </li>
            <li>
                <div class="collapsible-header"><i class="material-icons">lock</i>Privacy Policy</div>
                <div class="collapsible-body"><p id="pos"></p></div>
            </li>
        </ul>
    </div>
    <div class="modal-footer">
        <a href="#registerform" class="modal-action modal-close waves-effect waves-green btn-flat modal-trigger" onclick="openRegFormFromTOS()">Agree</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="buttonModalCancelTOS()">Decline</a>
    </div>
</div>


<!-- Register Dialog 2 -->
<div id="registerform" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4 class="center">Register Form</h4>
        <p class="center" id="helpMessage">All form fields are required</p>
        <div class="row">
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">face</i>
                    <input placeholder="Steve" id="firstName" type="text" class="validate">
                    <label for="firstName" data-error="invalid data given" data-success="">First Name</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">supervisor_account</i>
                    <input placeholder="O'Niel" id="lastName" type="text" class="validate">
                    <label for="lastName" data-error="invalid data given" data-success="">Last Name</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">email</i>
                    <input placeholder="example@example.com" id="email" type="email" class="validate">
                    <label for="email" data-error="invalid data given" data-success="">Email Address</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">send</i>
                    <input id="email2" type="email" class="validate">
                    <label for="email2" data-error="invalid data given" data-success="">Re-Enter Email Address</label>
                </div>
            </div>
            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">lock_outline</i>
                    <input placeholder="XXXXXX" id="passwordReg" type="password" class="validate">
                    <label for="passwordReg" data-error="invalid data given" data-success="">Password</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">lock</i>
                    <input id="passwordReg2" type="password" class="validate">
                    <label for="passwordReg2" data-error="passwords don't match" data-success="">Re-Enter Password</label>
                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer">
        <a href="#!" class="modal-action waves-effect waves-green btn-flat" onclick="registration()">Register</a>
        <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="buttomModalCancelRegister()">Cancel</a>
    </div>
</div>

<!-- Please wait message -->
<!-- We can close this modal with JS using $('#wait').closeModal(); -->
<div id="wait" class="modal wait">
    <div class="modal-content">
        <h5 class="center">Please wait</h5>
        <p class="center">Communicating with Server</p>
        <div class="progress">
            <div class="indeterminate"></div>
        </div>
    </div>
</div>

<!-- Mobile Forms -->
<div id="mobileTOS1" class="modal modal-sheet modal-fixed-footer">
    <div class="modal-content">
        <h4 class="center">Info</h4>
        <p class="center">By agreeing to register you agree to the following TOS, Privacy Policy and Security Policy</p>
        <ul class="collapsible z-depth-0" data-collapsible="expandable">
            <li>
                <div class="collapsible-header mobile"><i class="material-icons">subject</i>Terms and Conditions</div>
                <div class="collapsible-body"><p id="tos-mb"></p></div>
            </li>
            <li>
                <div class="collapsible-header mobile2"><i class="material-icons">lock</i>Privacy Policy</div>
                <div class="collapsible-body"><p id="pos-mb"></p></div>
            </li>
        </ul>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action waves-effect waves-green btn-flat modal-close" onclick="openRegFormFromTOSMobile()">Agree</a>
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat" onclick="buttonMobileCancelTOS()">Decline</a>
    </div>
</div>

<!-- Mobile Registration Form -->
<div id="registerFormMobile" class="modal modal-sheet modal-fixed-footer">
    <div class="modal-content">
        <h4 class="center">Register Form</h4>
        <p class="center" id="helpMessageMobile">All form fields are required</p>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">face</i>
                <input placeholder="Steve" id="firstNameMobile" type="text" class="validate">
                <label for="firstNameMobile" data-error="invalid data given" data-success="">First Name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">supervisor_account</i>
                <input placeholder="O'Niel" id="lastNameMobile" type="text" class="validate">
                <label for="lastNameMobile" data-error="invalid data given" data-success="">Last Name</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">email</i>
                <input placeholder="example@example.com" id="emailMobile" type="email" class="validate">
                <label for="emailMobile" data-error="invalid data given" data-success="">Email Address</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">send</i>
                <input id="email2Mobile" type="email" class="validate">
                <label for="email2Mobile" data-error="invalid data given" data-success="">Re-Enter Email Address</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">lock_outline</i>
                <input placeholder="XXXXXX" id="passwordRegMobile" type="password" class="validate">
                <label for="passwordRegMobile" data-error="invalid data given" data-success="">Password</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix">lock</i>
                <input id="passwordReg2Mobile" type="password" class="validate">
                <label for="passwordReg2Mobile" data-error="passwords don't match" data-success="">Re-Enter Password</label>
            </div>
        </div>
        <div class="row right">
            <a href="#!" class="modal-action modal-close waves-effect waves-green btn-flat" onclick="buttonMobileCancelReg()">Cancel</a>
            <a href="#!" class="modal-action waves-effect waves-green btn-flat">Register</a>
        </div>
    </div>
</div>

<!-- Please wait message -->
<!-- We can close this modal with JS using $('#wait').closeModal(); -->
<div id="waitMobile" class="modal modal-sheet">
    <div class="modal-content">
        <h5 class="center">Please wait</h5>
        <p class="center">Communicating with Server</p>
        <div class="progress">
            <div class="indeterminate"></div>
        </div>
    </div>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/pdm/verify.js"></script>
<script type="text/javascript" src="js/pdm/register.js"></script>
<script type="text/javascript" src="js/pdm/clearReg.js"></script>
<script type="text/javascript" src="js/pdm/clearLog.js"></script>
<script type="text/javascript" src="js/pdm/switchModal.js"></script>
<script type="text/javascript" src="js/pdm/switchModalMobile.js"></script>
<script type="text/javascript" src="js/pdm/loadTXT.js"></script>
<script type="text/javascript" src="js/pdm/closeCollapsible.js"></script>
<script type="text/javascript" src="js/pdm/login.js"></script>
<script type="text/javascript" src="js/main.js"></script>
</body>