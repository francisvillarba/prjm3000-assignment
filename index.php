<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 1/05/2016
 * Time: 6:42 PM
 */

session_start();

require('php/core/tools/verifyLoggedIn.php');        // Needed to check if they are logged in

// Check login
$checkLogin = new session();
$isLoggedIn = $checkLogin->verifySession( $_COOKIE[PHPSESSID] );
$result = !$isLoggedIn; // Use below if we always want to see the landing page
// $result = false;

$host = $_SERVER[ 'HTTP_HOST' ];
//echo $_COOKIE[PHPSESSID];
//echo '<br>';
//echo $result;
if ( $result ) {
    // Go to the index page
    header( 'HTTP/1.1 100 Continue');
    header( 'Location: ' . $_SERVER['REQUEST_URI'] . 'php/index.php' );


    echo "<p class='center'>Welcome back to Suggestion Box!</p>";
    echo "<p class='center'>Redirecting you in 3 seconds\n</p>";
    echo "<p class='center'>If you have not been redirected, click <a href='https://$host/php/index.php'>here</a></p>";
    exit();
} else {
    header( 'HTTP/1.1 401 Not Authorized');
    header( 'Location: ' . $_SERVER['REQUEST_URI'] . 'php/landing.php' );

    echo "<p class='center'>Redirecting you in 3 seconds\n</p>";
    echo "<p class='center'>If you have not been redirected, click <a href='https://$host/php/landing.php'>here</a></p>";
    exit();
}