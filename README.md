
# Team Members
---
    Francis Villarba
    ROLE: Developer (HTML5 / CSS / JS)
    ROLE: Scrum Master / Project Manager
    ROLE: Server Administrator (LAMP / Linux)
    ROLE: Documentation / Organisation
    ROLE: Graphic Designer    
    
    
    Nikhil Gudhka
    ROLE: Developer (General)
    ROLE: PHP Administrator
    ROLE: PHP Developer
    ROLE: Database Communications (JS / PHP / MySQL)
    ROLE: User Experience (Testing / Development)
    
    
    Ben Hutchinson
    ROLE: Developer (General)
    ROLE: Security Analyst
    ROLE: Security Administrator
    ROLE: Jack of all Trades (All languages)
    ROLE: Project Adviser


    Benjamin Koala Robinson
    ROLE: Developer (General)
    ROLE: Gamification Master
    ROLE: Pixel Artist
    ROLE: HTML Developer (HTML5)
    ROLE: Naming / Theming


    Emily Kenworthy
    ROLE: Developer (General)
    ROLE: Database Administrator (MySQL)
    ROLE: Database Developer (MySQL)
    ROLE: User Experience Testing
    ROLE: Cipher Master


    Eric Lin
    ROLE: Developer (General)
    ROLE: User Experience Developer (HTML5 / CSS)
    ROLE: User Experience Testing
    ROLE: Localisation / Translation (English / Chinese Trad.)