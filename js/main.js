/**
 * Here contains some of the proprietary js created for PRJM3000 by Francis Villarba
 * that was modified from various resources in the jQuery and Materialize Documentations
 *
 * @version 0.25
 * @since 26-March-2016
 */

/**
 * Enables a lot of the functionality used in MaterializeCSS theme
 * Waits for the page to be loaded first
 */
$(document).ready(function() {
    // Side navigation bar for mobiles / tablets
    $('.button-collapse').sideNav( {
        menuWidth: 300,
        edge: 'left',
        closeOnClick: true
    });

    // Parallax Image Effect
    $('.parallax').parallax();

    // Normal Modal Dialogs
    $('.modal-trigger').leanModal( {
        dismissible: false
    });

    // The wait modal dialog
    $('.modal-trigger-wait').leanModal( {
        dismissible: false
    });

    // The Terms and conditions modal dialog
    $('.modal-trigger-TOS').leanModal( {
        dismissible: true
    });

    // The message modal dialog
    $('.modal-trigger-message').leanModal( {
        dismissible: true
    });

    $(document).ready(function(){
        // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
        $('.modal-trigger-mobile-TOS').leanModal();
    });

    setTimeout(function(){
        $('body').toggleClass('loaded', true); // After 10 seconds, just show the darn page already
        $('h1').css('color','#222222');
    }, 10000);
});

/**
 * This function is for the pre-loading screen
 */
$(window).load ((function() {
    $(function() {
        $('body').toggleClass('loaded', true);
        // setTimeout( $('#loader-wrapper').remove(), 1000); // Disable this for now
    }); // Force to true
}));

/**
 * Scroll to the top of the page smoothly -- Requires jQuery
 */
$(".scroll-top").click( function() {
   $('html,body').animate( { // Duration of 1 second
       scrollTop: $(".top-page").offset().top }, 1000);
});

/**
 * Disable scrolling of web-page when a message / dialog is up
 */
function disableScroll() {
    //noinspection JSJQueryEfficiency
    $('body').css( { overflow: 'auto' } );
    //noinspection JSJQueryEfficiency -- This is because this is a bug fix
    $('body').css( { overflow: 'hidden !important' } );
}

/**
 * Re-enable scrolling of web-page when a message / dialog is closed
 */
function enableScroll() {
    $('body').css( { overflow: 'auto' } );
}