/**
 * Created by Ben Rogers on 4/04/2016.
 * Modified slightly by Francis Villarba to fix bugs
 */

var numCats = 0;
var currLevel = 1;

$(document).ready(function() {
    // Side navigation bar for mobiles / tablets
    $('.button-collapse').sideNav({
        menuWidth: 300,
        edge: 'right',
        closeOnClick: true
    });

    // Needed to show the logout dialog
    $('.modal-trigger').leanModal( {
        dismissible: true, // Modal can be dismissed by clicking outside of the modal
        opacity: .5 // Opacity of modal background
    });
});

/**
 * Causes the data within the submission to be validated and stored
 */
submitPost = function() {
    //Ben H
};

/**
 *  Loads the categories from file
 */
loadCategories = function() {
    var name = "category-";
    var cats = "";

    $.get("media/txt/categories.txt", function(data) {
        $(".result").html(data);
        cats = data;

        var catArr = cats.split(',');
        numCats = catArr.length;

        for(var i = 0; i < numCats; i++) {
            var id = i + 1;
            document.getElementById(String(name).concat(String(id))).innerHTML = catArr[i];
        }
    }, 'text');

    currLevel = 2;
    showCats();
};

/**
 *  Redirects the user to their profile page
 */
viewProfile = function() {
    //Francis - needs to link to user's profile
    var ua        = navigator.userAgent.toLowerCase(),
        isIE      = ua.indexOf('msie') !== -1,
        version   = parseInt(ua.substr(4, 2), 10);

    // Internet Explorer 8 and lower
    if (isIE && version < 9) {
        var link = document.createElement('a');
        link.href = "profile_mgmt.html";
        document.body.appendChild(link);
        link.click();
    }

    // All other browsers can use the standard window.location.href (they don't lose HTTP_REFERER like IE8 & lower does)
    else {
        window.location.href = "profile_mgmt.html";
    }
};

/**
 * Loads the posts for the category into their objects
 *
 * @param catID
 */
loadPosts = function(catID) {
    //Francis
    //Do SQL stuff based on cat id
    currLevel = 3;
    showPosts();
};

/**
 * Loads a specific post based on its number within the list
 *
 * @param postNum
 */
openPost = function(postNum) {
    //Francis
    //Do SQL stuff to load post + comments
    currLevel = 4;
};

/**
 * Hides mm buttons, should only be invoked by 'show' methods
 */
hideButtons = function() {
    document.getElementById("mm-post-icon").style.display = 'none';
    document.getElementById("mm-post-text").style.display = 'none';
    document.getElementById("mm-cat-icon").style.display = 'none';
    document.getElementById("mm-cat-text").style.display = 'none';
    document.getElementById("mm-profile-icon").style.display = 'none';
    document.getElementById("mm-profile-text").style.display = 'none';
    document.getElementById("back-arrow").style.display = 'block';
};

/**
 * Sets the page into the default main menu state
 */
showButtons = function() {
    document.getElementById("mm-post-icon").style.display = 'block';
    document.getElementById("mm-post-text").style.display = 'block';
    document.getElementById("mm-cat-icon").style.display = 'block';
    document.getElementById("mm-cat-text").style.display = 'block';
    document.getElementById("mm-profile-icon").style.display = 'block';
    document.getElementById("mm-profile-text").style.display = 'block';
    document.getElementById("back-arrow").style.display = 'none';
    hideCats();
    hidePosts();
};

/**
 * Hides categories, should only be invoked by 'show' methods
 */
hideCats = function() {
    var name = "category-";
    for(var i = 1; i <= numCats; i++) {
        document.getElementById(name.concat(String(i))).style.display = 'none';
    }
};

/**
 * Sets the page into the category state
 */
showCats = function() {
    var name = "category-";
    for(var i = 1; i <= numCats; i++) {
        document.getElementById(name.concat(String(i))).style.display = 'block';
    }
    hideButtons();
    hidePosts();
};

/**
 * Hides posts, should only be invoked by 'show' methods
 */
hidePosts = function() {
    var name = "post-";
    for(var i = 1; i <= 10; i++) {
        document.getElementById(name.concat(String(i))).style.display = 'none';
    }
};

/**
 * Sets the page into the posts state
 */
showPosts = function() {
    var name = "post-";
    for(var i = 1; i <= 10; i++) {
        document.getElementById(name.concat(String(i))).style.display = 'block';
    }
    hideButtons();
    hideCats();
};

/**
 * The logic behind the "back" arrow based on the current user "level"
 */
backArrow = function() {
    if(currLevel < 2) {

    }else if(currLevel == 2) {
        showButtons();
        hideCats();
        currLevel = 1;
    }else if(currLevel == 3) {
        showCats();
        currLevel = 2;
    }else if(currLevel = 4) {
        showPosts();
        currLevel = 3;
    }
};