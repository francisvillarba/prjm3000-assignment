/**
 * Created by francis on 30/03/2016.
 */

/**
 * What to do when the website has finished initialising (but not finished loading)
 */
$(document).ready(function() {

    // Enable the tab category bar
    $(document).ready(function(){
        $('ul.tabs').tabs();
    });

    // Enable the accordian
    $(document).ready(function(){
        $('.collapsible').collapsible({
            accordion : false // A setting that changes the collapsible behavior to expandable instead of the default accordion style
        });
    });

    // Enable the modal sheet dialog box
    $(document).ready(function(){
        // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
        $('.modal-trigger').leanModal();
    });

    // Show the loading screen
    setTimeout(function(){
        $('body').toggleClass('loaded', true); // After 10 second, just set it to true
        $('h1').css('color','#222222');
    }, 500);

});