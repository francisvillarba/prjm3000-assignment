/**
 * Created by Ben Rogers on 16/05/2016.
 */
var options = {
    classname: 'my-class',
    id: 'my-id',
    target: document.getElementById('bodyContent')
};

var nanobar = new Nanobar( options );

$(window).load(function() {
    this.nanobar.go( 50 );
    Materialize.showStaggeredList('#post-content');
    this.nanobar.go( 100 );
});

$(document).ready( function() {
    loadPosts();
});

/**
 *  Loads the posts from the database
 */
loadPosts = function() {
    //TODO here
};