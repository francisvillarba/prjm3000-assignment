/**
 * Here contains some of the proprietary js created for PRJM3000 by Francis Villarba
 * that was modified from various resources in the jQuery and Materialize Documentations
 *
 * This JS deals with the initialisation of elements for
 * @version 0.25
 * @since 26-March-2016
 */

$(document).ready(function() {

    // Show the login screen
    setTimeout(function(){
        $('body').toggleClass('loaded', true); // After 10 second, just set it to true
        $('h1').css('color','#222222');
    }, 10000);
});

$(window).load(function () {

    $(function () {
        $('body').toggleClass('loaded', true);
    }); // Force to true
});