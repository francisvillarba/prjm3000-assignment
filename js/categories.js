/**
 * Created by Ben Rogers on 29/04/2016.
 * Refactored by Francis Villarba 02/05/2016
 */
var options = {
    classname: 'my-class',
    id: 'my-id',
    target: document.getElementById('bodyContent')
};

var nanobar = new Nanobar( options );

$(window).load(function() {
    this.nanobar.go( 50 );
    Materialize.showStaggeredList('#category-content');
    this.nanobar.go( 100 );
});

$(document).ready( function() {
    loadCategories();
});

/**
 *  Loads the categories from file
 */
loadCategories = function() {
    var name = "category-";
    var cats = "";
    $.get("../../media/txt/categories.txt", function(data) {
        $(".result").html(data);
        cats = data;

        var catArr = cats.split(',');
        numCats = catArr.length;

        for(var i = 0; i < numCats; i++) {
            var id = i + 1;
            document.getElementById(String(name).concat(String(id))).innerHTML = "<p>".concat(catArr[i]).concat("</p>");
        }
    }, 'text');
};
