/**
 * Created by Francis Villarba on 2/05/2016.
 */

$(document).ready(function(){
    $('.startHidden').slideDown(500);
});

slideBody = function() {
    $( "#bodyContent" ).slideUp(500);
}

slideToSubmission = function() {
    slideBody();
    window.location.href = 'submissions.php';
}

slideToCategories = function() {
    slideBody();
    window.location.href = 'categories.php';
}

slideToProfile = function() {
    slideBody();
    window.location.href = 'profile.php';
}

slideToPosts = function() {
    slideBody();
    window.location.href = 'viewCategory.php';
}

slideToComments = function() {
    slideBody();
    window.location.href = 'viewPost.php';
}

slideToAdmin = function() {
    slideBody();
    window.location.href = '../admin/dashboard.php';
}