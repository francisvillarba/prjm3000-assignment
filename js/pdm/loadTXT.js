/**
 * Loads the TOS information from a text file on the server on demand (so we don't waste resources loading a very
 * large text file unless it is needed!
 *
 * Created by francis on 28/03/2016.
 * Version 0.3
 * Since 30-March-2016
 */

/** Desktop Versions here **/
loadTextTOS = function() {
    $("#tos").load("../media/txt/Terms_Conditions.txt");
};

loadTextPOS = function() {
    $("#pos").load("../media/txt/Privacy_Policy.txt");
};

/** General Functions Here **/

updateTOSText = function() {
    loadTextTOS();
    loadTextPOS();
};