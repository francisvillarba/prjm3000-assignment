/**
 * Bug fix for modal dialog issues on materialize graphics library
 *
 * The bug is where the div.lean-overlay object may not successfully remove it self
 * if there is a chain of modal document calls. This script aims to delete them.
 *
 * This version is for mobile phones -- Requires switchModal file as well
 *
 * @author Francis Villarba
 * @version 0.27
 * @since 27-March-2016
 *
 */

/**
 * What to do when we accept the TOS
 */
openRegFormFromTOSMobile = function() {
    $('div.lean-overlay').remove();
    closeTOSInfoMobile();
    clearMobileRegForm();
    switchModal();
    $('#registerFormMobile').openModal();
};

/**
 * What occurs when the click the cancel / decline button in the TOS
 */
buttonMobileCancelTOS = function() {
    $('#mobileTOS1').closeModal();
    $('div.lean-overlay').remove();
    enableScroll();
    closeTOSInfoMobile();
};

/**
 * What occurs if we click cancel on the registration form
 */
buttonMobileCancelReg = function() {
    $('#registerFormMobile').closeModal();
    $('div.lean-overlay').remove();
    clearMobileRegForm();
    enableScroll();
};

/**
 * Show wait dialog for mobile phones
 */
openWaitDiagMobile = function() {
    $('div.lean-overlay').remove();
    switchModal();
    $('#waitMobile').openModal({ dismissible: false}); // So they cannot close it
};