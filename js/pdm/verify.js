/**
 * Created by francis on 26/03/2016.
 */
/**
 * This code is used to verify the form data to ensure that it is correct
 *
 * Regex was sourced from https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address in order to
 * conform to industry standards for e0-mail addresses
 *
 * @Author: Francis Villarba
 * @version: 0.27
 * @since 26-March-2016
 *
 */

/**
 * This function verifies the form data then alerts the user if it is incorrect
 */
verifyForm = function () {
    /** Globals **/
    var firstName;
    var lastName;
    var nameRegex = /^[A-Z][-'a-zA-Z]+$/;// Allows for Andrew O'relly, Erin D'z'oza but not andrew or !@#$%^&*()
    var maxNameLength = 30;
    var minNameLength = 3;

    var email;
    var email2;
    var emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    var maxEmailLength = 80;
    var minEmailLength = 6;

    var password;
    var repassword;
    var passwordRegex = /^([0-9a-zA-Z])+$/;
    var minPasswordLength = 6;
    var maxPasswordLength = 256;

    /**
     * Checks the length of the string then outputs true or false if valid length
     * @param o the string to check
     * @param min the minimum length as an int
     * @param max the maximum length as an int
     * @returns {boolean} true if valid, false otherwise
     */
    function checkLength(o, min, max) {
        return !(o.length > max || o.length < min);
    }
    /**
     * Checks the regular expression
     * @param o the string to check
     * @param regex the regular expression to use
     * @returns {boolean} true if valid, false otherwise
     */
    function checkRegexp(o, regex) {
        var reg = new RegExp("regex");
        return (!( reg.test(o.valueOf()) ));
    }
    /**
     * Checks two strings if they are the same
     * @param o the first string object
     * @param o2 the second string object
     * @returns {boolean} true if same, false otherwise
     */
    function checkEquals(o, o2) {
        return (o.valueOf() === o2.valueOf() && o.valueOf().length === o2.valueOf().length );
    }

    /**
     * Checks the values if they are correct or not
     * @returns {boolean} true if correct, false otherwise
     * */
    function checkValues() {
        var valid = true;

        // Taken out for now as it was not working -- The other checks should suffice anyhow
/*        if (firstName == lastName == email == email2 == password == repassword == undefined ) {
            alert("Server Sent Error 412 \nThe registration information cannot be blank");
            return false;
        }*/
        // Check if they are of valid lengths first
        valid = valid && checkLength(firstName, minNameLength, maxNameLength);
        if ( !valid ) {
            alert( "Invalid Last Name length \nExpected length  " + minNameLength + "-" + maxNameLength + " given: " + firstName.valueOf().length );
            return false;
        }
        valid = valid && checkLength(lastName, minNameLength, maxNameLength);
        if ( !valid ) {
            alert( "Invalid Last Name length \nExpected length  " + minNameLength + "-" + maxNameLength + " given: " + lastName.valueOf().length );
            return false;
        }
        valid = valid && checkLength(email, minEmailLength, maxEmailLength);
        if ( !valid ) {
            alert( "Invalid Email length \nExpected length range  " + minEmailLength + "-" + maxEmailLength + " given: " + email.valueOf().length );
            return false;
        }
        valid = valid && checkLength(email2, minEmailLength, maxEmailLength);
        if ( !valid ) {
            alert( "Invalid Email length \nExpected length range " + minEmailLength + "-" + maxEmailLength + " given: " + email2.valueOf().length );
            return false;
        }
        valid = valid && checkLength(password, minPasswordLength, maxPasswordLength);
        if ( !valid ) {
            alert( "Invalid Password length \nExpected length range " + minPasswordLength + "-" + maxPasswordLength + " given: " + password.valueOf().length );
            return false;
        }
        valid = valid && checkLength(repassword, minPasswordLength, maxPasswordLength);
        if ( !valid ) {
            alert( "Invalid Password length \nExpected length range " + minPasswordLength + "-" + maxPasswordLength + " given: " + repassword.valueOf().length );
            return false;
        }

        // Check if they are valid
        valid = valid && checkRegexp(firstName, nameRegex);
        if ( !valid ) {
            alert( "Invalid Information given in first name field");
            return false;
        }
        valid = valid && checkRegexp(lastName, nameRegex);
        if ( !valid ) {
            alert( "Invalid Information given in last name field");
            return false;
        }
        valid = valid && checkRegexp(email, emailRegex);
        if ( !valid ) {
            alert( "Invalid Information given in email field");
            return false;
        }
        valid = valid && checkRegexp(email2, emailRegex);
        if ( !valid ) {
            alert( "Invalid Information given in email field");
            return false;
        }
        valid = valid && checkRegexp(password, passwordRegex);
        if ( !valid ) {
            alert( "Invalid Information given in password field");
            return false;
        }
        valid = valid && checkRegexp(repassword, passwordRegex);
        if ( !valid ) {
            alert( "Invalid Information given in password field");
            return false;
        }
        valid = valid && checkEquals(email, email2);
        if ( !valid ) {
            alert( "The email addresses do not match");
            return false;
        }
        valid = valid && checkEquals(password, repassword);
        if ( !valid ) {
            alert("The passwords do not match");
            return false;
        }
        return valid;
    }

    /**
     * Update the values in the script
     */
    function updateValues() {
        firstName = $("#firstName").val();
        lastName = $("#lastName").val();
        email = $("#email").val();
        email2 = $("#email2").val();
        password = $("#passwordReg").val();
        repassword = $("#passwordReg2").val();
    }

    updateValues();

    if( firstName == lastName == email == email2 == password == repassword )
    {
        // Not initialised values, we use the data from mobile instead
        firstName = $("#firstName-mb").val();
        lastName = $("#lastName-mb").val();
        email = $("#email-mb").val();
        email2 = $("#email2-mb").val();
        password = $("#passwordReg-mb").val();
        repassword = $("#passwordReg2-mb").val();
    }

    return checkValues();
};