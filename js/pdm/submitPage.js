/**
 * Created by Francis Villarba on 13/05/2016.
 * For the submissions page of the assignment
 */

/** Nanobar Stuff **********************************************************************/

var options =
{
    classname: 'my-class',
    id: 'my-id',
    target: document.getElementById('bodyContent')
};

var nanobar = new Nanobar( options );

/** jQuery Load Functions Stuff ********************************************************/

// Globals
var ready = false; // So we can tell if loading is finished

var tagsArray;

/**
 * What to do when the loading is finished
 */
finished = function()
{
    this.nanobar.go(100);
};

/**
 * Load Percentage
 * @param number int an integer representing the % of page load
 */
loadAmount = function( number )
{
    ready = true;
    this.nanobar.go( number )
};

$(window).load(function()
{
    if( !ready )
    {
        this.nanobar.go(50);
    }
    else {
        this.nanobar.go(100);
    }
    $('.startHidden').slideDown(500);
    $(document).ready(function()
    {
        $('textarea#sub-content').characterCounter();
    });
});

$(document).ready(function()
{
    nanobar.go(25);
    loadCategories();
    loadTags();
    finished();
});

/** PRJM3000 Specific Code Stuff *******************************************************/

/**
 *  Loads the categories from file
 *  Original by Ben R.
 *  Modified by Francis Villarba to work with Dynamically loading dropdown menus
 */
loadCategories = function() {
    var cats = "";
    var docRef = "category-picker";

    $.get("../../media/txt/categories.txt", function(data) {
        $(".result").html(data);
        cats = data;

        var catArr = cats.split(',');
        numCats = catArr.length;

        var select = document.getElementById( docRef );
        var id = 0;
        var option = null;

        for(var i = 0; i < numCats; i++) {
            option = document.createElement("option");
            option.value = i;
            option.innerHTML = catArr[i];
            select.appendChild( option );
            id++;
            //document.getElementById(String(name).concat(String(id))).innerHTML = catArr[i];
        }
        $('select').material_select();
        //loadAmount(75);
        finished();
    }, 'text');
};

/**
 * Loads tags from a file
 * Refactored from the Ben R's Idea for loading categories, but this time for
 * use with the tag-it JS Library
 */
loadTags = function() {
    var tags = "";
    $.get("../../media/txt/tags.txt", function( data ) {
        $(".result").html(data);
        tags = data;
    }, 'textTags');

    tagsArray = tags.split('\n');
    return tagsArray;
};

getTags = function() {
    return tagsArray;
};