/**
 * Created by francis on 28/03/2016.
 */

loginVerify = function( username, password ) {
    var emailRegex = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    var passwordRegex = /^([0-9a-zA-Z])+$/;

    var validUsername = emailRegex.test( username );
    var validPassword = passwordRegex.test( password );

    if ( validUsername == validPassword == true ) {
        return 1;
    } else {
        return 0; // False
    }
};

loginPHP = function( username, password ) {
    var responseOut = -1;
    var toSend = new Array( username, password );
    $.ajax( {
        cache: false,
        type: "POST",
        url: "core/login.php", // The file we are sending the request
        data: { LoginInfo: toSend }, // PHP sees { value 1 } and javascript sees {value 2}
        dataType: "text",
        timeout: 5000,
        async: false
    }).done ( function( response ) {
        responseOut = response;
    }).fail ( function( response ) {
        responseOut = response;
    });
    return responseOut;
};

loginRedirect = function() {
    location.assign("https://www.iforgetpdm.tk/~francis/php/index.php");
    window.location.replace( "https://www.iforgetpdm.tk/~francis/php/index.php" );
    window.location = "https://www.iforgetpdm.tk/~francis/php/index.php";
    window.location.href = "https://www.iforgetpdm.tk/~francis/php/index.php";
    $(location).attr('href', 'https://www.iforgetpdm.tk/~francis/php/index.php');
};

loginMain = function() {
    var username = $('#username').val();
    var password = $('#password').val();

    var valid = loginVerify( username, password );
    if ( valid ) {
        var error = loginPHP ( username, password );

        if ( error == 0 )
        {
            // alert( "Login successful!");
            console.log( "Login Successful" );
            loginRedirect();
        }
        else if ( error == 1 )
        {
            alert( "We do not recognise your login information\nPlease try again!");
            console.log( "Login Failed" );
            location.reload();
        }
        else if ( error == 2 ) {
            alert("Your account is banned!\nPlease contact the admin for further information");
            console.log("Login Failed");
            location.reload();
        }
        else if ( error == 3 )
        {
            alert( "We do not recognise your login information\nPlease try again!");
            console.log( "Login Failed" );
            location.reload();
        }
        else if ( error == 4 )
        {
            alert( "We do not recognise your login information\nPlease try again!");
            console.log( "Login Failed" );
            location.reload();
        }
        else
        {
            alert( "Unknown error has occured when logging in\nServer reports " + error );
            location.reload();
        }

    } else {
        alert( "Invalid Login Details Given\nPlease check your inputs then try again!");
        console.log("Rejected input as it fails the REGEX test");
        location.reload();
    }
};