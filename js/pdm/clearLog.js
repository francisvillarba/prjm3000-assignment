/**
 * Clears the login field after you have logged in, or when you have canceled the login
 * @author Francis Villarba
 * @version 0.1
 * @since 27-March-2016
 */

clearLoginForm = function() {
    //   $('#textarea1').val('New Text'); // The prototype to use when destroying fields
    $('#username').val("");
    $('#password').val("");
};
