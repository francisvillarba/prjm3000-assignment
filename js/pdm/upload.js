var submissionID;
var submitEvent = new CustomEvent('pushFiles');

/**
 * Causes the data within the submission to be validated and stored
 */
submitPost = function() {
    /*TODO:
     1. Check each entry exists
     2. Run database script to put entry in db
     3. Find submission ID
     4. Create folder
     5. Upload files using submission ID
     */
    submissionID = beginPost();
    if (submissionID == -1)
    {
        alert("Invalid Submission ID");
    }
    else
    {
        document.getElementById("submitButton").dispatchEvent(submitEvent);
    }
};

/* HEY FRANCIS THIS IS MEANT TO GIVE US SUBMISSION ID */
beginPost = function() {

    // Get the password and such that we will need
    var title = $("#sub-title").val();
    alert( title );
    var sessID = jQuery.cookie("PHPSESSID");

    alert( sessID );
    var category = "Miscellaneous";

    // Put them into an array for the PHP script to take :)
    var toSend = new Array( sessID, title, category );

    var toReturn = -1;

    // Send the data to the PHP script :D
    $.ajax({
        cache: false,
        type: "POST",
        url: "../core/tools/prepareSubmission.php", // The file we are sending the request towards
        data: { postPrepare : toSend }, // PHP sees { value 1 } and javascript sees {value 2}
        dataType: "text",
        timeout: 5000,
        async: false
    }).done( function ( result )  {
        toReturn = result;
    }).fail( function ( result ) {
        alert( result );
        toReturn = -1;
    });
    if ( toReturn != 0 ) {
        alert( toReturn );
        return -1;
    } else {
        alert( toReturn );
        return toReturn;
    }
};

Dropzone.options.dropzoneMain =
{
    maxFilesize: 20, //Mb
    maxFiles: 5,
    parallelUploads: 5,
    autoProcessQueue: false,

    init: function()
    {
        currDropzone = this;

        //TODO Submit button to process queue and set submissionID per call
         var submitButton = document.querySelector("#submitButton");
         submitButton.addEventListener("pushFiles", function()
         {
         currDropzone.processQueue();
         });

        currDropzone.on("sending", function(file, xhr, formData)
        {
            formData.append('submissionID', submissionID);
        });

        /*this.on("addedfile", function(file)
         {
         alert("File Added!");
         });*/
        currDropzone.on("maxfilesexceeded", function()
        {
            alert("Max files exceeded (5)")
        });

        currDropzone.on("queuecomplete", function()
        {
            alert("Files Uploaded!");
        });
    }
};