/**
 * Created by francis on 26/03/2016.
 */
/**
 * Registration script created by Francis Villarba for PDM / PRJM3000
 *
 * @version 0.15
 * @since 26-March-2016
 */

$(document).ready(function() {
    $('.modal-trigger').leanModal( {
        dismissible: false
    });
});

/** Desktop and Tablet Specific Code **********************************************************************************/
registration = function() {

    var valid = false;

    function sendData() {
        // Get the password and such that we will need
        var firstName = $("#firstName").val();
        var lastName = $("#lastName").val();
        var email = $("#email").val();
        var password = $("#passwordReg").val();

        // Put them into an array for the PHP script to take :)
        var toSend = new Array( firstName, lastName, email, password );

        var toReturn = -1;

        // Send the data to the PHP script :D
        $.ajax({
            cache: false,
            type: "POST",
            url: "core/registration.php", // The file we are sending the request
            data: { UserInfo : toSend }, // PHP sees { value 1 } and javascript sees {value 2}
            dataType: "text",
            timeout: 5000,
            async: false
        }).done( function (result)  {
            toReturn = result;
        }).fail( function (result) {
            alert( result );
            toReturn = -1;
        });

        if ( toReturn != 0 ) {
            alert( toReturn );
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * What to do at the end of the function call
     */
    function finish() {
        if ( valid ) {
            var regResult = sendData();
            $('#wait').closeModal();
            if ( regResult == 0 ) {
                alert("Registration successful!\nPlease try logging in now");
                // closeWait();
                // closeForm();
                //noinspection JSJQueryEfficiency
                // $('div.lean-overlay').remove();
                // clearRegForm();
                // //noinspection JSJQueryEfficiency
                // $('div.lean-overlay').remove();
                // clearLoginForm();
                // //noinspection JSJQueryEfficiency
                // $('div.lean-overlay').remove();
                // openLoginFromReg();
                location.assign("https://www.iforgetpdm.tk/~francis/php/landing.php");
                window.location.replace( "https://www.iforgetpdm.tk/~francis/php/landing.php" );
                window.location = "https://www.iforgetpdm.tk/~francis/php/landing.php";
                window.location.href = "https://www.iforgetpdm.tk/~francis/php/landing.php";
                $(location).attr('href', 'https://www.iforgetpdm.tk/~francis/php/landing.php');
            } else if ( regResult == 1 ) {
                alert("Registration was unsuccessful!\nPlease try again later");
                // closeWait();
                // reloadForm();
            } else { // Obviously if this occurs, SCREAM VERY LOUDLY
                alert("Unknown error has occurred in the registration!\nPlease try again later");
            }
        } else {
            alert("Registration form was rejected by server\nReject reason: Invalid data was given in the form!\nAction: Please check your form and try again");
            // closeWait();
            // reloadForm();
        }
    }

    /**
     * Verifies the registration information given
     * @returns {boolean} true if valid, false otherwise
     */
    function verifyInfo() {
        valid = verifyForm();
    }
    setTimeout( verifyInfo, 3000 ); // Delay to ensure server doesn't overload / They don't spam
    setTimeout( finish, 5000 ); // Long delay here
};

/** Mobile Specific Code **********************************************************************************************/
registrationMobile = function() {
    var valid = false;

    function sendData() {
        // Get the password and such that we will need
        var firstName = $("#firstName-mb").val();
        var lastName = $("#lastName-mb").val();
        var email = $("#email-mb").val();
        var password = $("#passwordReg-mb").val();

        // Put them into an array for the PHP script to take :)
        var toSend = new Array( firstName, lastName, email, password );

        var toReturn = -1;

        // Send the data to the PHP script :D
        $.ajax({
            cache: false,
            type: "POST",
            url: "core/registration.php", // The file we are sending the request
            data: { UserInfo : toSend }, // PHP sees { value 1 } and javascript sees {value 2}
            dataType: "text",
            timeout: 5000,
            async: false
        }).done( function (result)  {
            toReturn = result;
        }).fail( function (result) {
            alert( result );
            toReturn = -1;
        });

        if ( toReturn != 0 ) {
            alert( toReturn );
            return -1;
        } else {
            return 0;
        }
    }

    /**
     * What to do at the end of the function call
     */
    function finish() {
        if ( valid ) {
            var regResult = sendData();
            $('#wait').closeModal();
            if ( regResult == 0 ) {
                alert("Registration successful!\nPlease try logging in now");
                location.assign("https://www.iforgetpdm.tk/~francis/php/landing.php");
                window.location.replace( "https://www.iforgetpdm.tk/~francis/php/landing.php" );
                window.location = "https://www.iforgetpdm.tk/~francis/php/landing.php";
                window.location.href = "https://www.iforgetpdm.tk/~francis/php/landing.php";
                $(location).attr('href', 'https://www.iforgetpdm.tk/~francis/php/landing.php');
            } else if ( regResult == 1 ) {
                alert("Registration was unsuccessful!\nPlease try again later");
            } else { // Obviously if this occurs, SCREAM VERY LOUDLY
                alert("Unknown error has occurred in the registration!\nPlease try again later");
            }
        } else {
            alert("Registration form was rejected by server\nReject reason: Invalid data was given in the form!\nAction: Please check your form and try again");
        }
    }

    /**
     * Verifies the registration information given
     * @returns {boolean} true if valid, false otherwise
     */
    function verifyInfo() {
        valid = verifyForm();
    }
    setTimeout( verifyInfo, 3000 ); // Delay to ensure server doesn't overload / They don't spam
    setTimeout( finish, 5000 ); // Long delay here
};
