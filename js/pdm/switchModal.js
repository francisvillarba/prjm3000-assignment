/**
 * Bug fix for modal dialog issues on materialize graphics library
 *
 * The bug is where the div.lean-overlay object may not successfully remove it self
 * if there is a chain of modal document calls. This script aims to delete them.
 *
 * @author Francis Villarba
 * @version 0.27
 * @since 27-March-2016
 *
 */

/**
 * Switch Modal function -- helper to ensure that when the modal dialog switches over, the exclusivity is kept
 */
switchModal = function() {
    disableScroll();
};

/**
 * Show wait dialog
 */
openWaitDiag = function() {
    $('div.lean-overlay').remove();
    switchModal();
    $('#wait').openModal({ dismissible: false}); // So they cannot close it
};

/**
 * Open the Terms of Service Agreement (Pre-register dialog) from the login page
 * when the user clicks register
 */
openTosFromLogin = function() {
    $('#loginDialog').closeModal();
    $('div.lean-overlay').remove();
    clearLoginForm();
    switchModal();
    $('#registerTOS').openModal();
    updateTOSText();
};

/**
 * Open the Terms of Service Agreement (Pre-register dialog) from the login page
 * when the user clicks register
 */
openLoginFromReg = function() {
    $('div.lean-overlay').remove();
    clearLoginForm();
    switchModal();
    $('#loginDialog').openModal();
};

/**
 * What occurs when you click accept from the Terms of Service Agreement
 */
openRegFormFromTOS = function() {
    $('#registerTOS').closeModal();
    $('div.lean-overlay').remove();
    switchModal();
    $('#registerform').openModal();
};

/**
 * What occurs when the click the cancel / decline button in the TOS
 */
buttonModalCancelTOS = function() {
    $('#registerTOS').closeModal();
    $('div.lean-overlay').remove();
    enableScroll();
    closeTOSInfo();
};

/**
 * What occurs when we click cancel on the login page
 */
buttonModalCancelLogin = function() {
    $('#registerTOS').closeModal();
    $('div.lean-overlay').remove();
    clearLoginForm();
    enableScroll();
};

/**
 * What occurs when we click cancel on the cancel button on the registration form
 */
buttomModalCancelRegister = function() {
    $('#registerform').closeModal();
    $('div.lean-overlay').remove();
    clearRegForm();
    enableScroll();
};
