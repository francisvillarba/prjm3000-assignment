/**
 * Created by francis on 28/03/2016.
 */

closeTOSInfo = function() {
    //noinspection JSJQueryEfficiency
    if ( $("div.collapsible-header").hasClass("active") ) {
        $("div.collapsible-header").click();
    }
};

closeTOSInfoMobile = function() {
    //noinspection JSJQueryEfficiency
    if ( $("div.collapsible-header.mobile").hasClass("active") ) {
        $("div.collapsible-header.mobile").click();
    }
    //noinspection JSJQueryEfficiency
    if ( $("div.collapsible-header.mobile2").hasClass("active") ) {
        $("div.collapsible-header.mobile2").click();
    }
};