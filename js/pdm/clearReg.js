/**
 * Clears the registration form if the user decides to cancel
 *
 * @author Francis Villarba
 * @version 0.1
 * @since 27-March-2016
 *
 */
clearRegForm = function() {
    //   $('#textarea1').val('New Text'); // The prototype to use when destroying fields

    $('#firstName').val("");
    $('#lastName').val("");
    $('#email').val("");
    $('#email2').val("");
    $('#passwordReg').val("");
    $('#passwordReg2').val("");
};

clearMobileRegForm = function() {
    $('#firstNameMobile').val("");
    $('#lastNameMobile').val("");
    $('#emailMobile').val("");
    $('#email2Mobile').val("");
    $('#passwordRegMobile').val("");
    $('#passwordReg2Mobile').val("");
};