<?php
/**
 * This is a PHP refactor of submission.html that was created by Ben R and Ben H
 * to allow better support for submitting
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 10/05/2016
 * Time: 7:56 PM
 */

session_start();

?>
<head>
    <!-- Information for the Site -->
    <meta charset="UTF-8">

    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../css/pdm/submission.css">

    <link href="../css/dropzone.css" type="text/css" rel="stylesheet" />

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<nav class="top-page green darken-4 z-depth-0">
    <div class="pin-top nav-wrapper">
        <a href="#" class="brand-logo center">Create Submission</a>

        <ul class="right">
            <li><a href="#"><i class="material-icons">close</i></a></li>
        </ul>
    </div>
</nav>

<!--Submission area -->
<div class="container">
    <div class="section">

        <p>Title:</p><textarea maxlength="80" placeholder="Enter a title" required="required" name="title_input"></textarea><br><br>
        <p>Keywords:</p><textarea maxlength="20" placeholder="Enter a new keyword" name="keyword_input"></textarea><br><br>
        <p>Content:</p><textarea style="height:200px" maxlength="1000" placeholder="Enter submission text here" required="required" name="content_input"></textarea><br>

        <p style="text-align:right">0/1000</p><br>
        <P>DROPZONE</P>
        <div class="col m12">
            <form action="../php/upload.php" class="dropzone dropzone-previews" id="dropzoneMain"></form>
        </div>

        <!-- Button used to start upload of dropzone files, linked to upload.js -->
        <button type="button" id="submitButton" onclick="beginPost()">Upload</button>
    </div>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/materialize.js"></script>
<script type="text/javascript" src="../js/init.js"></script>
<script type="text/javascript" src="../js/index.js"></script>
<script type="text/javascript" src="../js/vendor/dropzone.js"></script>
<script type="text/javascript" src="../js/pdm/upload.js"></script>
</body>