<?php
$imageFileTypes = ["png", "jpg", "tif", "gif", "bmp"];
$videoFileTypes = ["webm", "mkv", "flv", "vob", "avi", "mov", "mpg", "mpeg4", "m4v", "rm", "rmvb", "wmv"];
$documentFileTypes = ["docx", "doc", "pdf", "log", "msg", "odt", "pages", "rtf", "tex", "wpd", "txt", "wps"];

$imageDir = "/uploads/images";
$videoDir = "/uploads/videos";
$documentDir = "/uploads/documents";
$fileDir = "/uploads/files";

$target_dir = "/uploads/temp";
$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
$uploadOk = 1;
$fileType = pathinfo($target_file,PATHINFO_EXTENSION);

// Check if file already exists
if (file_exists($target_file))
{
    //echo "Sorry, file already exists.";
    echo 1;
    $uploadOk = 0;
}
// Check file size is less than 20Mb
if ($_FILES["fileToUpload"]["size"] > 20000000)
{
    //echo "Sorry, your file is too large (20Mb Max).";
    echo 1;
    $uploadOk = 0;
}

// Allocate file to location
if (in_array($fileType, $imageFileTypes))
{
    $target_dir = $imageDir;
}
elseif (in_array($fileType, $videoFileTypes))
{
    $target_dir = $videoDir;
}
elseif (in_array($fileType, $documentFileTypes))
{
    $target_dir = $documentDir;
}
else
{
    $target_dir = $fileDir;
}

// Check if $uploadOk is set to 0 by an error
if ($uploadOk == 0)
{
    echo 1;
}
// if everything is ok, try to upload file
else
{
    if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
    {
        //echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
        echo 0;
    }
    else
    {
        echo 1;
    }
}
?>