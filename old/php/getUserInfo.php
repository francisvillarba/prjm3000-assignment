<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 18/04/2016
 * Time: 8:43 PM
 */

require('connectDatabase.php');

$uID = $_GET['id'];

$stmt = "SELECT getUser('@uID')";

clearStoredResults($connection);

$result = mysqli_query ( $connection, $stmt );
if ( !$result ) {
    die('CALL procedure failed: (' . $connection->errno . ') ' . $connection->error );
} else {
    while ($row = $result->fetch_assoc()) {
        printf ("%s \n", $row[0]);
    }
}

#------------------------------------------
function clearStoredResults($connection){
#------------------------------------------
    while($connection->next_result()){
        if($l_result = $connection->store_result()){
            $l_result->free();
        }
    }
}