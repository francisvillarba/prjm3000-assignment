<?php
/**
 * Created by PhpStorm.
 * User: nikhilgudhka
 * Date: 7/04/2016
 * Time: 04:16 PM
 */

session_start();  //start a session

unset($_SESSION['email']);

session_destroy();  //destroy the session

echo 'Successfully Logged Out';
header('Location :index.html'); //redirect to index.html

exit();