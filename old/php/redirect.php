<?php
// Created by Francis Villarba for redirection
$redirect = $_GET[ 'r' ];
$host = $_SERVER[ 'HTTP_HOST'];
header( 'Refresh: 5; URL=https://$host/$redirect' );

echo "Redirecting you in 5 seconds\n";
echo "If you have not been redirected, click <a href='https://$host/$redirect'>here</a>";
exit();
?>