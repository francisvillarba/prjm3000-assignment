<?php

/**
 * Refreshable header
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 19/04/2016
 * Time: 2:17 PM
 */

session_start(); // This is needed on all of the pages

?>

<!-- Navigation Bar -->
<nav class="top-page teal z-depth-0">
    <div class="pin-top nav-wrapper"> <!-- &emsp; makes a tab :P -->
        <a class="brand-logo center"><?php echo $name;?></a>
        <ul class="right">
            <li><a href="" onclick="location.reload();"><i class="material-icons">refresh</i></a></li>
            <li><a href="#!" ><i class="material-icons">close</i></a></li>
        </ul>
    </div>
</nav>

