<!-- Created in PhpStorm by Francis Villarba on the 7/04/2016 at 1:02 PM -->
<?php
session_start(); // This is needed on all of the pages

//TODO - Get the name, if no name defined, init to blank
$name = $_GET['n'];

?>

<!DOCTYPE html>
    <!-- For Modernizr to check browser support -->
    <!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
    <!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
    <!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
    <!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

    <head>
        <!-- Information for the Site -->
        <meta charset="UTF-8">
        <title>Suggestion Box</title>

        <meta name="keywords" content="iForget, PDM, PRJM3000, Curtin University, Project, Suggestions, Suggestion-Box">
        <meta name="description"
              content="Suggestion-Box - A project providing students and educators a new way to share and communicate ideas, suggestions and concepts">

        <!-- Style Stuff -->
        <link type="text/css" rel="stylesheet" href="../../css/materialize-fonts.css"/>
        <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
        <link type="text/css" rel="stylesheet" href="../../css/main.css"/>
        <link type="text/css" rel="stylesheet" href="../../css/loading.css"/>

        <!-- Responsive Design + Compatibility -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Removed apple-touch-icon since this is a sub-module -->
        <script src="../../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>

    <body>
        <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
            your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Navigation Bar / Header -->
        <nav class="top-page teal z-depth-0">
            <div class="pin-top nav-wrapper"> <!-- &emsp; makes a tab :P -->
                <a class="brand-logo center"><?php echo $name ?></a>
                <ul class="right">
                    <li><a href="" onclick="location.reload();"><i class="material-icons">refresh</i></a></li>
                </ul>
            </div>
        </nav>

        <!-- It is generally best practise to load javascript at the end to decrease load times! -->
        <script type="text/javascript" src="../../js/vendor/jquery-1.11.2.js"></script>
        <script type="text/javascript" src="../../js/materialize.js"></script>
    </body>
</html>
