<?php
/**
 * The main header for the website, this assumes it is loaded in a wrapper PHP method
 * and will not work on it's own!
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 30/04/2016
 * Time: 5:22 PM
 */

session_start(); // This is needed on all of the pages
$indexRoot = $_SERVER['SERVER_NAME'];

?>
<!-- Navigation Bar -->
<nav class="top-page green darken-4 z-depth-0">
    <div class="pin-top nav-wrapper"> <!-- &emsp; makes a tab :P -->
        <ul class="center">
            <!-- For now using this, revert to the commented out version once migrated to the final -->
            <a onclick="location.assign( 'https://www.iforgetpdm.tk/~francis' );" class="brand-logo center waves-effect waves-ripple waves-light">
<!--            <a onclick="location.assign( 'https://<?php //echo $indexRoot; ?>//' );" class="brand-logo center waves-effect waves-ripple waves-light"> -->
                <?php echo $name;?>
            </a>
        </ul>
        <ul class="left">
            <li>
                <a onclick="history.back();" class="waves-effect waves-light" >
                    <i class="material-icons">keyboard_backspace</i>
                </a>
            </li>
        </ul>
        <ul class="right">
            <li>
                <a href="#" data-activates="sidemenu-left-lg" class="button-collapse show-on-large show-on-med-and-down">
                    <i class="medium material-icons" style="color:white;">menu</i>
                </a>
            </li>
        </ul>
    </div>
</nav>

<ul id="sidemenu-left-lg" class="side-nav show-on-large show-on-med-and-down">
    <!-- The Main portion of the menu section -->
    <li class="bold">
        <a href="#!" id="nav-profile-lg">My Profile</a>
    </li>
    <li>
        <a href="#logout-confirm" class="modal-trigger" id="nav-logout-lg">Log Out</a>
    </li>

    <!-- The sub menus :D -->
    <ul class="collapsible collapsible-accordion">
        <li class="bold">
            <a class="collapsible-header waves-effect waves-green">Sort By</a>
            <div class="collapsible-body">
                <ul>
                    <li>
                        <a href="#" onclick="Materialize.toast('Sorting by A-Z', 1500)">Alphabetical A-Z</a>
                    </li>
                    <li>
                        <a href="#" onclick="Materialize.toast('Sorting by Z-A', 1500)">Alphabetical Z-A</a>
                    </li>
                    <li>
                        <a href="#" onclick="Materialize.toast('Sorting by Newest First', 1500)">Newest First</a>
                    </li>
                    <li>
                        <a href="#" onclick="Materialize.toast('Sorting by Oldest First', 1500)">Oldest First</a>
                    </li>
                </ul>
            </div>
        </li>
    </ul>
</ul>