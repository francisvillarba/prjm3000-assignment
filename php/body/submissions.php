<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 2/05/2016
 * Time: 3:41 PM
 */

session_start();

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

?>

<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="" xmlns="http://www.w3.org/1999/html"> <!--<![endif]-->
<head>
    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../../css/vendor/materialize-tags.css"/>

    <!-- PRJM3000 Specific Stuff -->
    <link type="text/css" rel="stylesheet" href="../../css/pdm/index_body.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/submit_body.css"/>

    <link href="../../css/dropzone.css" type="text/css" rel="stylesheet" />

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>

<!-- Main content -->
<h3 class="center-align">Create a new submission</h3>

<!-- Desktop Version TODO -- Make a mobile version of this -->
<div id="bodyContent" class="container startHidden">

    <div class="row"> <!-- The Submission form itself -->
        <form name="submission" class="col s12">
            <div class="row"> <!-- The Title of the Post -->
                <div class="input-field col s6">
                    <i class="material-icons prefix">title</i>
                    <input id="sub-title" type="text" length="80">
                    <label for="sub-title">Title</label>
                </div>

                <div class="input-field col s6"> <!-- The Subject / Category of the post -->
                    <i class="material-icons prefix">subject</i>
                    <select id="category-picker">
                        <option value="" disable selected>Chose a category</option>
                    </select>
                </div>
            </div>

            <div class="row"> <!-- Tags area of the form -->
                <div class="input-field col s12">
                    <i class="material-icons prefix">local_offer</i>
                    <input id="tags" type="text" data-role="materialtags">
                    <label for="tags">Submission Tags</label>
                </div>
            </div>

            <div class="row"> <!-- The main body of the submission -->
                <div class="input-field col s12">
                    <i class="material-icons prefix">line_weight</i>
                    <textarea id="sub-content" class="materialize-textarea" length="1000"></textarea>
                    <label for="sub-content">Submission Content</label>
                </div>
            </div>
        </form>

        <!-- Additions that is outside the form -->
        <div class="row"> <!-- Attachment Area -->
            <div class="col s12" style="align-items:center">
                <form action="../upload.php" class="dropzone" id="dropzoneMain"></form>
            </div>
        </div>

        <div class="row"> <!-- Submit Button -->
            <button class="btn-large waves-effect waves-light right green darken-3" id="submit-action" name="submit-action" onclick="beginPost()">Submit
                <i class="material-icons right">send</i>
            </button>
        </div>
    </div>
</div>

<!-- Mobile Version TODO -- Make a mobile version of this-->


<!-- It is generally best practise to load javascript at the end to decrease load times! -->

<!-- Core -->
<script type="text/javascript" src="../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../../js/vendor/jquery.cookie.js"></script>

<script type="text/javascript" src="../../js/vendor/nanobar.js"></script>

<!-- UI -->
<script type="text/javascript" src="../../js/materialize.js"></script>
<script type="text/javascript" src="../../js/vendor/materialize-tags.js"></script>

<!-- The scripts that are custom for our project -->
<script type="text/javascript" src="../../js/pdm/submitPage.js"></script>

<script type="text/javascript" src="../../js/vendor/dropzone.js"></script>
<script type="text/javascript" src="../../js/pdm/upload.js"></script>

<input type="file" multiple="multiple" class="dz-hidden-input" style="visibility: hidden; position: absolute; top: 0px; left: 0px; height: 0px; width: 0px;">
</body>
</html>