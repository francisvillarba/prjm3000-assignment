<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 3/05/2016
 * Time: 12:32 AM
 */

session_start();
?>

<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/index_body.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/list_style.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<!-- New version of main content -->
<div id="bodyContent" class="container startHidden">
    <ul id="post-content" class="collection" style="border: 0">
        <li href="#!" id="post-1" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-2" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-3" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-4" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-5" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-6" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-7" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-8" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-9" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
        <li href="#!" id="post-10" class="collection-item" style="opacity: 0" onclick="slideToComments()">Loading...</li>
    </ul>

    <div class="row">
        <div class="col s2 m2">
            <a href="#!" class="waves-effect waves-light green darken-3 btn-flat white-text left">Prev</a>
        </div>
        <div class="col s8 m8">
            <p style="text-align: center">Posts: 1-10</p>
        </div>
        <div class="col s2 m2">
            <a href="#!" class="waves-effect waves-light green darken-3 btn-flat white-text right">Next</a>
        </div>
    </div>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../../js/materialize.js"></script>
<script type="text/javascript" src="../../js/vendor/nanobar.js"></script>
<script type="text/javascript" src="../../js/posts.js"></script>
<script type="text/javascript" src="../../js/body_transition.js"></script>
</body>
</html>

