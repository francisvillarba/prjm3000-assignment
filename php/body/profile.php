<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 2/05/2016
 * Time: 6:53 PM
 */

session_start();

?>

<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/index_body.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<!-- Main content -->
<div id="bodyContent" class="container startHidden">
    <p>This is where Profile view is supposed to go</p>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../../js/materialize.js"></script>
<script type="text/javascript" src="../../js/init.js"></script>
<script type="text/javascript" src="../../js/profile.js"></script>
</body>
</html>
