<?php
/**
 * Created by PhpStorm.
 * User: Ben Rogers
 * Date: 16/05/2016
 * Time: 5:56 PM
 */
?>
<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/index_body.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/comment.css">

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<div id="bodyContent" class="container">
    <h4 style="text-align: center; text-shadow: 0 0 6px #88ff88;">Pikachu Discussion</h4>

    <div class="container main">
        <table>
            <th class="col s12 m10">
                <h5>Rare Pepe <img src="../../media/img/stars/star_4.png" alt="Rank 4" class="star_badge" style="width:20px; height:20px; color:green;"></h5>
                <p>Now, this is a story all about how my life got flipped-turned upside down and I'd like to take a minute
                    just sit right there I'll tell you how I became the prince of a town called Bel-Air
                </p>
            </th>
        </table>
    </div>

    <br>

    <div class="container main">
        <table>
            <th class="col s0 m2 bad hide-on-med-and-down"><h2>-2.5</h2></th>
            <th class="col s12 m10">
                <h5>Earthworm Jim <img src="../../media/img/stars/star_0.png" alt="Rank 0" class="star_badge" style="width:20px; height:20px; color:green;"></h5>
                <p>This is my really dope comment, it contains so much insight into the workings of this topic.
                    I had to add more text to see how well it would flow over onto the second line, you should hover on the star badge and check out the sweet colour change.
                    Here's even more text to test even more weird things because HTML is derpy at times.
                </p>
            </th>
        </table>
    </div>

    <br>

    <div class="container main" style="align-content:">
        <table>
            <th class="col s0 m2 good hide-on-med-and-down"><h2>7.6</h2></th>
            <th class="col s12 m10">
                <h5>Spyro the Dragon <img src="../../media/img/stars/star_7.png" alt="Rank 7" class="star_badge" style="width:20px; height:20px; color:green;"><span class="hide-on-large-only green-text text-darken-1" style="font-size: 0.7em;">&nbsp7.6</span></h5></span>
                <p>Earthworm Jim, that was so uncalled for man. How does your comment contribute anything to this discussion? Your comment should
                    be deleted because it is not welcome here. Hey admins, please take that down.</p>
            </th>
        </table>
    </div>

    <br>

    <div class="container main">
        <table>
            <th class="col s0 m2 neutral hide-on-med-and-down"><h2>0.2</h2></th>
            <th class="col s12 m10">
                <h5>Zeus <img src="../../media/img/stars/star_admin.png" alt="Rank A" class="star_badge" style="width:20px; height:20px; color:green;"></h5>
                <p>Something something angry lightning and thunder from the mountains of Olympus, I shall smite thee Earthworm Jim!!</p>
            </th>
        </table>
    </div>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../../js/materialize.js"></script>
<script type="text/javascript" src="../../js/vendor/nanobar.js"></script>

</body>
</html>
