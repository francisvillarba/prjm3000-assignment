<?php
/**
 * The main body of the index page of the assignment :)
 * Refactored from Ben Roger's version as a conversion from HTML (Ben's) to PHP (this one)
 * Thanks Ben!
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 2/05/2016
 * Time: 3:32 PM
 */

session_start();

?>
<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>

    <link type="text/css" rel="stylesheet" href="../../css/pdm/index_body.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/index.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <script src="../../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>

<!-- Main content -->
<div id="bodyContent" class="container startHidden">
    <div class="row" style="align-content:center; padding-top:50px;">
        <div class="col s12 m4 center">
            <i id="mm-post-icon" onclick="slideToSubmission()" class="large material-icons waves-effect waves-light" style="text-shadow:2px 2px 4px #333333;">mode_edit</i>
            <p id="mm-post-text">Create Post</p>
        </div>
        <div class="col s12 m4 center">
            <i id="mm-cat-icon" onclick="slideToCategories()" class="large material-icons waves-effect waves-light" style="text-shadow:2px 2px 4px #333333;">list</i>
            <p id="mm-cat-text">Categories</p>
        </div>
        <div class="col s12 m4 center">
            <i id="mm-profile-icon" onclick="slideToProfile()" class="large material-icons waves-effect waves-light" style="text-shadow:2px 2px 4px #333333;">person_pin</i>
            <p id="mm-profile-text">My Profile</p>
        </div>
    </div>
    <div class="row" style="align-content:center; padding-top:50px;">
        <div class="col s12" style="text-align: center;">
            <?php include('../admin/buttons/dashboard_link.php'); ?>
        </div>
    </div>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../../js/materialize.js"></script>
<script type="text/javascript" src="../../js/init.js"></script>
<script type="text/javascript" src="../../js/index.js"></script>
<script type="text/javascript" src="../../js/body_transition.js"></script>
</body>
</html>