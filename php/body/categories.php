<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 2/05/2016
 * Time: 3:41 PM
 */

session_start();

?>
<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>
    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/index_body.css"/>
    <link type="text/css" rel="stylesheet" href="../../css/pdm/list_style.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>

<body>

<!-- Main content -->
<!--<div id="bodyContent" class="startHidden">-->
<!--    <!--Categories + Sub-Categories-->
<!--    <!--Note that sub-categories have been removed-->
<!--    <div class="container">-->
<!--        <div class="col s12 center">-->
<!--            <p id="category-1"></p>-->
<!--            <p id="category-2"></p>-->
<!--            <p id="category-3"></p>-->
<!--            <p id="category-4"></p>-->
<!--            <p id="category-5"></p>-->
<!--            <p id="category-6"></p>-->
<!--            <p id="category-7"></p>-->
<!--            <p id="category-8"></p>-->
<!--            <p id="category-9"></p>-->
<!--            <p id="category-10"></p>-->
<!--            <p id="category-11"></p>-->
<!--            <p id="category-12"></p>-->
<!--            <p id="category-13"></p>-->
<!--            <p id="category-14"></p>-->
<!--        </div>-->
<!--    </div>-->
<!--</div>-->

<!-- New version of main content -->
<div id="bodyContent" class="container">
    <ul id="category-content" class="collection" style="border: 0">
        <li href="#!" id="category-1" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-2" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-3" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-4" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-5" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-6" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-7" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-8" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-9" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-10" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-11" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-12" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-13" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
        <li href="#!" id="category-14" class="collection-item" style="opacity: 0" onclick="slideToPosts()">Loading...</li>
    </ul>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../../js/materialize.js"></script>
<script type="text/javascript" src="../../js/vendor/nanobar.js"></script>
<script type="text/javascript" src="../../js/categories.js"></script>
<script type="text/javascript" src="../../js/body_transition.js"></script>
</body>
</html>
