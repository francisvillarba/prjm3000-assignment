<?php
/**
 * PHP conversion of BenYarrr aka SimonLuigi / Ben Roger's index.html for the online platform
 * Thanks Benjamin Rogers for your hard work!
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 30/04/2016
 * Time: 4:59 PM
 */

session_start(); // Required
$name = 'Suggestion Box';
// $indexRoot = __DIR__;

// TODO -- Check if they are logged in, and if they aren't redirect them to the welcome landing page

?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>

    <!-- Information for the Site-->
    <meta charset="UTF-8">
    <title>Suggestion Box</title>

    <meta name="keywords" content="iForget, PDM, PRJM3000, Curtin University, Project, Suggestions, Suggestion-Box">
    <meta name="description" content="Suggestion Box - A project providing students and educators a new way to share and communicate ideas, suggestions and concepts">

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css" media="screen,projection"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

    <!-- CSS related to main content pages -->
    <link type="text/css" rel="stylesheet" href="../css/pdm/index_body.css"/>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<?php
// The website header!
include(__DIR__.'/headers/main.php');
?>
<iframe id="frame" src="body/home.php">Your browser does not support iFrames. Please <a href="http://browsehappy.com/">upgrade your browser</a></iframe>

<!-- Modal Dialogs -->

<div id="logout-confirm" class="modal modal-fixed-footer"> <!-- Logout Dialog -->
    <div class="modal-content">
        <h4>Logout</h4>
        <p>Are you sure you want to logout?</p>
    </div>
    <div class="modal-footer">
        <a href="logout.php" class="modal-action modal-close waves-effect waves-green btn-flat green">Yes</a>
        <a class="modal-action modal-close waves-effect waves-red btn-flat red">No</a>
    </div>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/materialize.js"></script>
<script type="text/javascript" src="../js/index.js"></script>
</body>
</html>
