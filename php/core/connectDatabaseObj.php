<?php
/**
 * Refactored version of Nikhil's Connect Database php script to be compatible with OO style PHP
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 18/04/2016
 * Time: 11:04 PM
 */

global $connection;

class dbConnect {

    protected static $hostname='localhost'; // server name
    protected static $username='indb'; //mysql username
    protected static $password='some_pass_curiosity123'; //mysql password
    protected static $database='prjm3000'; // database name

    /**
     * Connect to the database
     *
     * @return bool false on failure / mysqli object on success
     */
    public function connect() {

        $connection = mysqli_connect
        (
            self::$hostname, self::$username,
            self:: $password, self::$database
        );

        // If we were not able to connect successfully, BLOW UP (jokes)
        if ( $connection === false ) {
            echo 'Could not connect to DB';
            echo '$connection->error()';
            return false;
        }

        // We assume connection was successful so therefore
        // TODO - Remove debug script
        // echo mysqli_get_host_info( $connection ). "\n";
        return $connection;
    }
    
    /**
     * Connect to the database in phpStorm (for testing remotely)
     * TODO -- Remove this later
     */
    public function connectStorm() {

        $connection = mysqli_connect
        (
            '45.55.168.11', self::$username, self:: $password,
            self::$database, 3306
        );

        // If we were not able to connect successfully, BLOW UP (jokes)
        if ( $connection === false ) {
            echo 'Could not connect to DB';
            echo '$connection->error()';
            die( 'Database connection failed: ' . mysqli_connect_error());
        }

        if (mysqli_connect_errno())
        {
            echo 'Failed to connect to MySQL: ' . mysqli_connect_error();
            //you need to exit the script, if there is an error
            exit();
        }

        // We assume connection was successful so therefore
        // TODO - Remove debug script
        // echo mysqli_get_host_info( $connection ). "\n";
        return $connection;
    }
}