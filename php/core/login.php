<?php
/**
 * Created by PhpStorm.
 * User: nikhilgudhka
 * Date: 3/26/16
 * Time: 2:18 PM
 */
Include('connectDatabase.php');

ini_set('display_errors','On');
error_reporting(E_ALL);

session_start();

$request = $_POST['LoginInfo']; //get array from JS

$password=$request[1]; //store the array index 1 into password
$hash = hash('sha256',$password); //hash the password with sha256 to check against the db password since db stores hashed password

$sql = "SELECT verifyLogin('$request[0]','$hash')"; // call the sql function verifyLogin
if( $result = mysqli_fetch_row( mysqli_query( $connection, $sql ) ) ) //connect to db and retrieve result
{

    if( $result[0]==0 ) //if 0 then login successful
    {
        $_SESSION['email'] = $request[0];
        // TODO -- Verify session code is missing in mysql
//            $checkIfExists = mysqli_query( $connection, "CALL verifySession('$_COOKIE[PHPSESSID]');");
//            if ( $checkIfExists === 1 ) {
//
//                // Add a new session
//                $res="CALL insSession('$_COOKIE[PHPSESSID]');";
//                $r=mysqli_query($connection,$res);
//            }

        // Add session cookie to DB
        $res="CALL insSession('$_COOKIE[PHPSESSID]');";
        $r=mysqli_query($connection,$res);

        // Get their User ID
        $uidQueryRes = mysqli_fetch_row( mysqli_query( $connection, "SELECT getID('$request[0]');") );
//            echo $uidQueryRes[0];
        $userID = $uidQueryRes[0];

        // Add their UserID to their session on DB
        $fin="CALL addUserToSession('$_COOKIE[PHPSESSID]','$userID');";
        $final = mysqli_query($connection, $fin);

        if ( $final )
        {
            echo '0';
        }
        else
        {
            echo '5';
            echo 'Unable to create session information';
            echo "Your Session: $_COOKIE[PHPSESSID] ";
            echo "Your UID: $uidQueryRes[0] -- $userID";
            echo mysqli_error( $connection );
        }
    }
    else if( $result[0]==1 ) //if 1 then login unsuccessful
    {
        echo '1';
    }
    else if( $result[0]==2 ) //if 2 they are banned
    {
        echo '2';
    }
    else if($result[0] == -1 )
    {
        echo '3';
    }
    else if( $result[0] == -2 )
    {
        echo '4';
    }
    else
    {
        echo '5';
        echo 'Unexpected result given from DB';
    }

}


