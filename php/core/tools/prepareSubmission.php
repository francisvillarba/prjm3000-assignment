<?php
/**
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 9/05/2016
 * Time: 5:41 PM
 */

// Basic initialisation
Include('../../core/connectDatabase.php');
$request = $_POST['postPrepare'];

//echo "PHP: Your sessID is - $request[0]\n";

$sqlQueryUID = "SELECT getIDFromSession('$request[0]')";
$RawUIDResult = mysqli_query( $connection, $sqlQueryUID );

//print_r( $RawUIDResult );

//echo "\n";
//echo "Test 1 - OO";
//echo "\n";
//print_r ( $RawUIDResult->fetch_row() );

//echo "\n";
//echo "Test 2 - Proc";
//echo " \n";

$result = mysqli_fetch_row( $RawUIDResult );

//print_r ($result);
//print_r( $result[0] );

// print_r ($result[0]); --
//TODO -- Now we definitely have the UserID, time to get the Submission ID

// Other idea -- $sql = "SELECT insSubmission('$uid','$request[0]','$request[2]');";
$sql = "CALL insSubmission('$uid','$request[0]','$request[2]');";
$halfResult = mysqli_query( $connection, $sql );
$halfResult2 = mysqli_fetch_row( $halfResult );
echo $halfResult2[0];

//echo "\n";
//echo "Test 3 - OO Style 2";
//echo "\n";
//$result2 = $RawUIDResult->fetch_row();
//print_r ($result2);

//Include('../../core/connectDatabase.php');
//
//$request = $_POST['postPrepare'];
//
//$uid = '12345';
//
//$sql = "CALL insSubmission('$uid','$request[0]','$request[2]')";
//
//if( $result = mysqli_fetch_row( mysqli_query( $connection, $sql ) ) )
//{
//
//}

//class prepareSubmission
//{
//    // Store connection information on this object for use
//    public static $connection;
//
//    public function __construct()
//    {
//        require_once(__DIR__.'/../connectDatabaseObj.php');
//        $dbConnection = new dbConnect();
//        self::$connection = $dbConnection->connect();
//    }
//
//    private function query( $stmt )
//    {
//        return mysqli_query( self::$connection, $stmt );
//    }
//
//    private function select( $stmt )
//    {
//        $rows = array();
//        $result = $this -> query( $stmt );
//        while ($row = $result -> fetch_assoc())
//        {
//            $rows[] = $row;
//        }
//        return $rows;
//    }
//
//    private function error()
//    {
//        return self::$connection->error();
//    }
//
//    public function getSID( $sessionID )
//    {
//        $stmt = "SELECT getIDFromSession('$sessionID')";
//        $result = $this->select( $stmt );
//        if ( $result === NULL )
//        {
//            echo $this->error();
//            return false;
//        }
//        else
//        {
//            return $result;
//        }
//    }
//
//    // This function assumes we have a valid session already
//    public function prepare( $request )
//    {
//        // request[0] == sessionID
//        // request[1] == title
//        // request[2] == category
//
//        $userIDQuery = "SELECT getIDFromSession('$request[0]')";
//        $userIDResult = $this->select( $userIDQuery );
//
//        // Check for errors
//        if ( $userIDResult === NULL )
//        {
//            echo $this->error();
//            return -1;
//        }
//        else if ( $userIDResult === -1 )
//        {
//            echo 'The userID does not exist';
//            return -1;
//        }
//
//        // Now get the submissionID
//        $sql = "SELECT insSubmission('$userIDResult','$request[1]', '$request[2]')";
//        $sID = $this->query( $sql );
//
//        if ( $sID === NULL )
//        {
//            echo $this->error();
//            return -1;
//        }
//        else if ( $sID === -1 )
//        {
//            echo 'The submissionID was malformed';
//            return -1;
//        }
//        else
//        {
//            return $sID;
//        }
//    }
//}