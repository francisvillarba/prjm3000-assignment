<?php
/**
 * Object that will get the user information from the DB then return it
 * to the calling php function
 *
 * Created by PhpStorm.
 * User: francis
 * Date: 18/04/2016
 * Time: 10:37 PM
 */

session_start(); // This is needed on all of the pages

class userInfo {

    // Store connection information on this object for use
    public static $connection;

    /**
     * Create a connection to the DB
     * Remember constructor needs double _'s to work
     */
    public function __construct()
    {
        require_once(__DIR__.'/../../core/connectDatabaseObj.php');
        $dbConnection = new dbConnect();
        self::$connection = $dbConnection->connect();
        // print_r (self::$connection = $dbConnection->connect() );
    }

    /**
     * Runs a query on the DB and returns the result
     *
     * @param $stmt String The statement to run
     * @return mixed The result of the mysqli::query() function
     */
    private function query( $stmt )
    {
        return mysqli_query( self::$connection, $stmt );
    }

    /**
     * Fetches rows from the DB ( using the SELECT query )
     *
     * @param $stmt String The statement to run
     * @return array Database rows on success, False boolean if failed
     */
    private function select( $stmt )
    {
        $rows = array();
        $result = $this -> query( $stmt );
        while ($row = $result -> fetch_assoc())
        {
            $rows[] = $row;
        }
        return $rows;
    }

    /**
     * Fetch the last error from the database
     *
     * @return string Database error message
     */
    private function error()
    {
        //TODO -- Check this code to ensure it works!
        return self::$connection->error();
    }

    /**
     * Runs the getUserInfo Script
     *
     * @param $userID String The userID of the person to get the data from
     * @return array Database rows
     */
    public function getUserInfo( $userID )
    {
        $stmt = "SELECT getUser('$userID')";
        $result = $this->select( $stmt );
        if ( $result === NULL )
        {
            echo $this->error();
            return false;
        }
        else
        {
            return $result;
        }
    }
}