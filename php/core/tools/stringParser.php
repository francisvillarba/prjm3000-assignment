<?php
/**
 * Object that can do a variety of things to a string when constructed and return the
 * information to the calling php function
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 19/04/2016
 * Time: 8:38 AM
 */

class parser
{
    // Store the string on this object for use
    private static $string;

    /**
     * parser constructor.
     * @param $string string the string to operate on
     */
    public function __construct( $string )
    {
        \parser::$string = $string;
    }


    /**
     * parses a string and returns an array
     * @param $delimiter string the delimiter to use on the string
     * @return array the array representation of the string
     */
    public function getArray( $delimiter )
    {
        // Check if null
        if ( \parser::$string  === false || !isset(\parser::$string) ) {
            echo 'String is null ';
        }
        // Create the array
        $array = explode( $delimiter, \parser::$string );

        // print_r($array);
        return $array;
    }
}