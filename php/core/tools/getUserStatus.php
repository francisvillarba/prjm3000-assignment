<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 21/04/2016
 * Time: 5:45 PM
 */

session_start();

class userStatus
{

    // Store connection information on this object for use
    public static $connection;

    /**
     * Create a connection to the DB
     * Remember constructor needs double _'s to work
     */
    public function __construct()
    {
        require_once(__DIR__.'/../../core/connectDatabaseObj.php');
        $dbConnection = new dbConnect();
        $this::$connection = $dbConnection->connect();
        // print_r (self::$connection = $dbConnection->connect() );
    }

    /**
     * Runs a query on the DB and returns the result
     *
     * @param $stmt String The statement to run
     * @return mixed The result of the mysqli::query() function
     */
    private function query( $stmt )
    {
        return mysqli_query( $this::$connection, $stmt );
    }

    /**
     * Converts the mysqli_result into a single value for use
     * @param $stmt string the mysql statement as a string
     * @param $mID string the supposed moderatorID code
     * @return string the return value
     */
    private function getValue( $stmt, $mID )
    {
        $status = $this->query( $stmt );

        // Convert into an array
        $rows = array();
        while ($row = $status -> fetch_assoc())
        {
            $rows[] = $row;
        }

        $test = $rows[0]; // Get the singular value
        $test2 = $test["getStatus('$mID')"];

//        // Debug -- print array
//        echo '<pre>';
//        print_r ( $rows );
//        echo '</pre>';
//
//        // Debug -- Attempt to print the singular values
//        echo '<br>';
//        print_r ( $test );
//        echo '<br>';
//        print_r ( $test2 );
//        echo '<br>';

        return $test2;
    }

    public function getStatus( $mID )
    {
        $stmt = "SELECT getStatus('$mID')";
        $result = $this->getValue( $stmt, $mID );
        mysqli_close( $this::$connection );
        return $result;
    }
}