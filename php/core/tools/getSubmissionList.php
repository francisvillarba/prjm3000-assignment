<?php
/**
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 20/05/2016
 * Time: 11:40 AM
 *
 * This is used to get the submissions array from the database, based on category etc.
 * NOTE the DB is limited to only 100 results
 *
 */

class getSubmissionList
{
    public static $connection;

    /**
     * Create a connection to the DB
     * Remember constructor needs double _'s to work
     */
    public function __construct()
    {
        require_once(__DIR__ . '/../../core/connectDatabaseObj.php');
        $dbConnection = new dbConnect();
        self::$connection = $dbConnection->connect();
        // print_r (self::$connection = $dbConnection->connect() );
    }

    /**
     * Runs a query on the DB and returns the result
     *
     * @param $stmt String The statement to run
     * @return mixed The result of the mysqli::query() function
     */
    private function query( $stmt )
    {
        return mysqli_query( self::$connection, $stmt );
    }


    /** Submission Getter Code Below **********************************************************************************/
    public function getSubmissions()
    {
        // Get the category name
        $category = $_GET['c'];
        $stmt = "";
    }
}