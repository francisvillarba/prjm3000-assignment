<?php
/**
 * Checks if the user has been logged in before allowing resources to be loaded
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 20/04/2016
 * Time: 10:18 PM
 */

/**
 * The Procedural Way, but we need to have it in OO style!
 *
require('../connectDatabaseObj.php');           // We need a connection to the DB in order to do this

// Get the details of the user and connect to the DB
$requester = $_COOKIE['PHPSESSID'];             // Who is requesting these details?
$dbConnection = new dbConnect();
$connection = $dbConnection->connect();

$stmt = "SELECT sessionID FROM Sessions WHERE sessionID='$requester'";
$result = mysqli_query( $connection, $stmt );

// If we have a set that is greater than 0, then we return true
if ( mysql_num_rows( $result ) != 0) { return true; }
// Otherwise they don't exist!
return false;
 *
 */

class session
{
    // Store connection information on this object for use
    public static $connection;

    /**
     * Create a connection to the DB
     * Remember constructor needs double _'s to work
     */
    public function __construct()
    {
        require_once(__DIR__.'/../../core/connectDatabaseObj.php');
        $dbConnection = new dbConnect();
        self::$connection = $dbConnection->connect();
        // print_r (self::$connection = $dbConnection->connect() );
    }

    /**
     * Runs a query on the DB and returns the result
     *
     * @param $stmt String The statement to run
     * @return mixed The result of the mysqli::query() function
     */
    private function query( $stmt )
    {
        return mysqli_query( self::$connection, $stmt );
    }

    public function verifySession( $sessionInfo )
    {
        $stmt = "SELECT verifySession('$sessionInfo');";
        $r = mysqli_fetch_row( mysqli_query( self::$connection, $stmt ) );

        if( $r === false ) // If the DB Command Failed
        {
            echo 'Error occurred';
            return false;
        }
        return $r[0];
    }
}