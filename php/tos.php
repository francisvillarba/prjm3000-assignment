<?php
/**
 * Terms of Agreements Page -- Refactored from About.php (known as index.html.second)
 * Created by PhpStorm.
 * User: Francis and Nikhil
 * Date: 15/05/2016
 * Time: 8:52 PM
 */

session_start();
include('loaders/loader.php');
?>

<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html lang="en">
<head>
    <!-- Information for the Site -->
    <meta charset="UTF-8">
    <title>Suggestion-Box</title>

    <meta name="keywords" content="iForget, PDM, PRJM3000, Curtin University, Project, Suggestions, Suggestion-Box">
    <meta name="description"
          content="Suggestion-Box - A project providing students and educators a new way to share and communicate ideas, suggestions and concepts">

    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../css/main.css"/>
    <link type="text/css" rel="stylesheet" href="../css/pdm/landing.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>

<div class="container valign-wrapper">
    <div id="Prompt" class="card-panel hoverable valign"> <!-- Main body div -->
        <!-- Note to self - put content inside of here -->
        <h4 class="center">Before you register</h4>
        <p class="center">By agreeing to register you agree to the following TOS, Privacy Policy and Security Policy</p>
        <ul class="collapsible z-depth-0" data-collapsible="expandable">
            <li>
                <div class="collapsible-header"><i class="material-icons">subject</i>Terms and Conditions</div>
                <div class="collapsible-body"><p id="tos"></p></div>
            </li>
            <li>
                <div class="collapsible-header"><i class="material-icons">lock</i>Privacy Policy</div>
                <div class="collapsible-body"><p id="pos"></p></div>
            </li>
        </ul>
        <div class="row">
            <a href="landing.php" class="waves-effect waves-light grey btn-flat white-text left">Decline</a>
            <a href="register.php" class="waves-effect waves-light green darken-3 btn-flat white-text right">Agree</a>
        </div>
    </div>
</div>

<!-- Scripts -->
<script type="text/javascript" src="../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/materialize.js"></script>
<script type="text/javascript" src="../js/pdm/verify.js"></script>

<!-- TOS Specific Code -->
<script type="text/javascript" src="../js/pdm/register.js"></script>
<script type="text/javascript" src="../js/pdm/loadTXT.js"></script>
<script type="text/javascript" src="../js/pdm/tos.js"></script>

</body>
</html>