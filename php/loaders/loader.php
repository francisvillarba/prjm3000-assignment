<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 19/04/2016
 * Time: 3:52 PM
 */

session_start();

?>
<script type="text/javascript" src="../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/loader.js"></script>
<link type="text/css" rel="stylesheet" href="../css/loading_transparent.css"/>

<div id="loader-wrapper">
    <div id="loader"></div>
</div>