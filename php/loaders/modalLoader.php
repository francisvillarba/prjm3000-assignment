<?php
/**
 * Otherwise known as quick loader!
 *
 * Created by PhpStorm.
 * User: francis
 * Date: 19/04/2016
 * Time: 4:00 PM
 */

session_start();

?>

<link type="text/css" rel="stylesheet" href="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../css/loading_modal.css"/>

<div id="loader-wrapper">
    <div id="loader"></div>
</div>

<script type="text/javascript" src="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../js/init_manageUser.js"></script>