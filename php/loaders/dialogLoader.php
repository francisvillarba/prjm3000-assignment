<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 19/04/2016
 * Time: 4:33 PM
 */

session_start();
?>

<link type="text/css" rel="stylesheet" href="../../../css/materialize-fonts.css"/>
<link type="text/css" rel="stylesheet" href="../../../css/materialize.min.css" media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="../../../css/main.css"/>

<body>

<h5 class="center">Please wait</h5>
<p class="center">Communicating with Server</p>
<div class="progress">
    <div class="indeterminate"></div>
</div>

<script type="text/javascript" src="../../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../../../js/materialize.js"></script>
<script type="text/javascript" src="../../../js/main.js"></script>
</body>
