<?php
/**
 * Registration page -- Refactored from About.php (known as index.html.second)
 * Created by PhpStorm.
 * User: Francis and Nikhil
 * Date: 15/05/2016
 * Time: 8:52 PM
 */

session_start();
include('loaders/loader.php');

?>

<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<html lang="en">
<head>
    <!-- Information for the Site -->
    <meta charset="UTF-8">
    <title>Suggestion-Box</title>

    <meta name="keywords" content="iForget, PDM, PRJM3000, Curtin University, Project, Suggestions, Suggestion-Box">
    <meta name="description"
          content="Suggestion-Box - A project providing students and educators a new way to share and communicate ideas, suggestions and concepts">

    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../css/main.css"/>
    <link type="text/css" rel="stylesheet" href="../css/pdm/landing.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>

<div class="container valign-wrapper show-on-medium-and-up hide-on-small-and-down">
    <div id="Prompt" class="card-panel hoverable valign"> <!-- Main body div -->
        <!-- Note to self - put content inside of here -->
        <h4 class="center">Register Form</h4>
        <p class="center" id="helpMessage">All form fields are required</p>
        <form class="col s12">

            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">face</i>
                    <input placeholder="Steve" id="firstName" type="text" class="validate">
                    <label for="firstName" data-error="invalid data given" required="true">First Name</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">supervisor_account</i>
                    <input placeholder="O'Niel" id="lastName" type="text" class="validate">
                    <label for="lastName" data-error="invalid data given" required="true">Last Name</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">email</i>
                    <input placeholder="example@example.com" id="email" type="email" class="validate">
                    <label for="email" data-error="invalid data given" required="true">Email Address</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">send</i>
                    <input id="email2" type="email" class="validate">
                    <label for="email2" data-error="invalid data given" required="true">Re-Enter Email Address</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s6">
                    <i class="material-icons prefix">lock_outline</i>
                    <input placeholder="XXXXXX" id="passwordReg" type="password" class="validate">
                    <label for="passwordReg" data-error="invalid data given" required="true">Password</label>
                </div>
                <div class="input-field col s6">
                    <i class="material-icons prefix">lock</i>
                    <input id="passwordReg2" type="password" class="validate">
                    <label for="passwordReg2" data-error="passwords don't match" required="true">Re-Enter Password</label>
                </div>
            </div>

            <div class="row"> <!-- The buttons -->
                <a href="landing.php" class="waves-effect waves-light grey btn-flat white-text">Cancel</a>
                <a class="modal-trigger waves-effect waves-light green darken-3 btn-flat white-text right" href="#wait" onclick="registration()">Register</a>
            </div>
        </form>
    </div>
</div>

<!-- Mobile version of the registration form -->
<div class="container valign-wrapper hide-on-med-and-up show-on-small show-on-small-and-down">
    <div id="Prompt" class="card-panel hoverable valign"> <!-- Main body div -->
        <!-- Note to self - put content inside of here -->
        <h4 class="center">Register Form</h4>
        <p class="center" id="helpMessage">All form fields are required</p>
        <form class="col s12">

            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix">face</i>
                    <input placeholder="Steve" id="firstName-mb" type="text" class="validate">
                    <label for="firstName" data-error="invalid data given" required="true">First Name</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix">supervisor_account</i>
                    <input placeholder="O'Niel" id="lastName-mb" type="text" class="validate">
                    <label for="lastName" data-error="invalid data given" required="true">Last Name</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix">email</i>
                    <input placeholder="example@example.com" id="email-mb" type="email" class="validate">
                    <label for="email" data-error="invalid data given" required="true">Email Address</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix">send</i>
                    <input id="email2-mb" type="email" class="validate">
                    <label for="email2" data-error="invalid data given" required="true">Re-Enter Email Address</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix">lock_outline</i>
                    <input placeholder="XXXXXX" id="passwordReg-mb" type="password" class="validate">
                    <label for="passwordReg" data-error="invalid data given" required="true">Password</label>
                </div>
            </div>

            <div class="row">
                <div class="input-field col s12">
                    <i class="material-icons prefix">lock</i>
                    <input id="passwordReg2-mb" type="password" class="validate">
                    <label for="passwordReg2" data-error="passwords don't match" required="true">Re-Enter Password</label>
                </div>
            </div>

            <div class="row"> <!-- The buttons -->
                <a href="landing.php" class="waves-effect waves-light grey btn-flat white-text">Cancel</a>
                <a class="modal-trigger waves-effect waves-light green darken-3 btn-flat white-text right" href="#wait" onclick="registrationMobile()">Register</a>
            </div>
        </form>
    </div>
</div>

<div id="wait" class="modal">
    <div class="modal-content">
        <h5 class="center">Please wait</h5>
        <p class="center">Communicating with Server</p>
        <div class="progress">
            <div class="indeterminate"></div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script type="text/javascript" src="../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/materialize.js"></script>

<!-- Register Related Scripts -->
<script type="text/javascript" src="../js/pdm/verify.js"></script>
<script type="text/javascript" src="../js/pdm/register.js"></script>
</body>
</html>