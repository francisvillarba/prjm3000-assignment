<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 20/05/2016
 * Time: 3:21 PM
 */


session_start(); // initialize session

require('core/logout.php');

session_destroy(); // destroy session
setcookie("PHPSESSID","",time()-3600,"/");

echo" <script type=\"text/JavaScript\"> setTimeout(\"location.href = 'http://www.iforgetpdm.tk/~francis/';\",3000); </script> ";

?>
<head>
    <link type="text/css" rel="stylesheet" href="../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../css/pdm/landing.css"/>
</head>
<html>
<body>

<!--<h5 class="center">Logout</h5>-->
<!--<p class="center">Communicating with Server</p>-->
<!--<div class="progress">-->
<!--    <div class="indeterminate"></div>-->
<!--</div>-->

<div class="container valign-wrapper">
    <div id="Prompt" class="card-panel hoverable valign"> <!-- Main body div -->
        <div class="row center">
            <h4 class="center">Suggestion Box</h4>
            <p class="center">Logging out! Please Wait</p>

            <!-- Loading Spinner -->
            <div class="preloader-wrapper big active center">
                <div class="spinner-layer spinner-blue center">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-red center">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-yellow center">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>

                <div class="spinner-layer spinner-green center">
                    <div class="circle-clipper left">
                        <div class="circle"></div>
                    </div><div class="gap-patch">
                        <div class="circle"></div>
                    </div><div class="circle-clipper right">
                        <div class="circle"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/materialize.js"></script>
<script type="text/javascript" src="../js/pdm/landing.js"></script>
</body>
</html>
