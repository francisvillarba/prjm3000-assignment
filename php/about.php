<?php
// set the expiration date to one hour ago
session_start();
//setcookie("PHPSESSID", "1234567890", time() + 1440, "/", "iforgetpdm.tk", true );
?>

<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

<head>
    <!-- Information for the Site -->
    <meta charset="UTF-8">
    <title>Suggestion Box</title>

    <meta name="keywords" content="iForget, PDM, PRJM3000, Curtin University, Project, Suggestions, Suggestion-Box">
    <meta name="description"
          content="Suggestion-Box - A project providing students and educators a new way to share and communicate ideas, suggestions and concepts">

    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../css/main.css"/>
    <link type="text/css" rel="stylesheet" href="../css/loading.css"/>
    <link type="text/css" rel="stylesheet" href="../css/about.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div id="loader-wrapper">
    <div id="loader"></div>
    <div class="loader-section section-left"></div>
    <div class="loader-section section-right"></div>
</div>

<!-- Navigation Bar / Header -->
<nav class="top-page green darken-4" role="navigation" id="top">
    <div class="nav-wrapper container">
        <!-- What the desktops and HiDPI large screens will see -->
        <a id="logo-container center" href="#" class="brand-logo center">iForget</a>
    </div>
</nav>

<!-- Initial Slice -->
<div id="index-banner" class="parallax-container">
    <div class="section no-pad-bot">
        <div class="container">
            <br><br>
            <div class="row center">
                <br><br>
                <h1 class="header center white-text text-lighten-1"><strong>Suggestion Box</strong></h1>
                <h5 class="header col s12 white-text text-shadow">A modern take on student to educator communications</h5>
            </div>
            <div class="row center">
                <!-- TODO - Change this to go to our new registration page! -->
                <a href="landing.php" id="desktopStarted" class="btn-large waves-effect waves-light teal lighten-1 hide-on-med-and-down">Get started</a>
            </div>
            <br><br>
        </div>
    </div>
    <div class="parallax"><img src="../media/img/about/001.gif" alt="Students and Teachers"></div>
</div>

<!-- Second Slice -->
<div class="container">
    <div class="section">
        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">flash_on</i></h2>
                    <h5 class="center">Bring on the ideas</h5>
                    <p class="light center">
                        Our aim is to give students a platform and place to voice their ideas quickly, easily and effortlessly through iForget's innovative Suggestion Box online platform.
                        <br><br>
                        iForget strives to empower students with a clear voice and gives educators the chance to hear it all!
                    </p>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">group</i></h2>
                    <h5 class="center">User Experience Focused</h5>
                    <p class="light center">
                        By utilizing elements and principles of modern design, we were able to create a framework that incorporates components and animations that provide clear feedback to users.
                        <br><br>
                        Through the use of a single underlying system across all platforms this allows for a unified and pleasing experience.
                    </p>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">settings</i></h2>
                    <h5 class="center">Easy to work with</h5>
                    <p class="light center">
                        We have provided detailed documentation as well as examples to help new users get started.
                        <br><br>
                        iForget is always open to feedback and can answer any questions a user may have about the Suggestion Box platform.
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Third Slice -->
<div class="parallax-container valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <h5 class="header col s12 light text-shadow"><b>Easy to integrate and with thoughtful security measures</b></h5>
            </div>
        </div>
    </div>
    <div class="parallax"><img src="../media/img/about/002.gif" alt="Security-Droid"></div>
</div>

<!-- Fourth Slice -->
<div class="container">
    <div class="section">
        <!--   Icon Section   -->
        <div class="row">
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">account_balance</i></h2>
                    <h5 class="center">Ease of Customisation</h5>
                    <p class="light center">
                        Suggestion Box is easy to customise.
                        <br><br>
                        With clear and concise documentation to logical placement of elements and resources, you can integrate Suggestion Box quickly and easily into your organisation.
                    </p>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">aspect_ratio</i></h2>
                    <h5 class="center">Self Contained</h5>
                    <p class="light center">
                        The Suggestion Box platform is independent and does not require external dependencies to run.
                        <br><br>
                        Just add PHP and MySQL and you're all set to go. IT teams - breathe that sigh of relief.
                    </p>
                </div>
            </div>
            <div class="col s12 m4">
                <div class="icon-block">
                    <h2 class="center green-text"><i class="material-icons">security</i></h2>
                    <h5 class="center">Secure</h5>
                    <p class="light center">
                        With extensive input validation and data encryption, combined with the self contained nature of the platform and support for SSL.
                        <br><br>
                        Rest assured, the platform has security down packed
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Fifth Slice -->
<div class="parallax-container final valign-wrapper">
    <div class="section no-pad-bot">
        <div class="container">
            <div class="row center">
                <a class="btn-large btn-large waves-effect waves-light teal scroll-top" href="#top">Back to top</a>
            </div>
            <br>
        </div>
    </div>
    <div class="parallax"><img src="../media/img/about/003.gif" alt="Students and Teachers again!"></div>
</div>

<!--<!-- Final Slice -->
<!--<div class="parallax-container valign-wrapper">-->
<!--    <div class="section no-pad-bot">-->
<!--        <div class="container">-->
<!--            <div class="row center">-->
<!--                <h5 class="header col s12 light text-shadow"><b>Scroll down more to find out more about iForget</b><br><br><br></h5>-->
<!--                <a class="btn-large btn-large waves-effect waves-light teal scroll-top" href="#top">Back to top</a>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
<!--    <div class="parallax"><img src="../media/img/about/Earth-and-Moon.jpg" alt="Earth and Sky Parallax Image"></div>-->
<!--</div>-->

<!-- Footer -->
<footer class="page-footer green darken-4">
    <div class="container">
        <div class="row">
            <div class="col l6 s12">
                <h5 class="white-text">iForget PDM Group</h5>
                <p class="grey-text text-lighten-4">We are a small, close-nit group of friends that banded together with the goal to change the world through new ideas, experiences and innovation.</p>
            </div>
            <div class="col l4 offset-l2 s12">
                <h5 class="white-text">More about iForget</h5>
                <ul>
                    <!-- TODO -- Fill these links in -->
                    <li><a class="grey-text text-lighten-3" href="#!">The Team</a></li>
                    <li><a class="grey-text text-lighten-3" href="#!">References</a></li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container center">
            <p class="white-text">&copy; 2016 iForget PDM Group & Curtin University of Western Australia</p>
        </div>
    </div>
</footer>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/materialize.js"></script>
<script type="text/javascript" src="../js/pdm/verify.js"></script>
<script type="text/javascript" src="../js/pdm/register.js"></script>
<script type="text/javascript" src="../js/pdm/clearReg.js"></script>
<script type="text/javascript" src="../js/pdm/clearLog.js"></script>
<script type="text/javascript" src="../js/pdm/switchModal.js"></script>
<script type="text/javascript" src="../js/pdm/switchModalMobile.js"></script>
<script type="text/javascript" src="../js/pdm/loadTXT.js"></script>
<script type="text/javascript" src="../js/pdm/closeCollapsible.js"></script>
<script type="text/javascript" src="../js/pdm/login.js"></script>
<script type="text/javascript" src="../js/main.js"></script>
</body>