<?php

/**
 * The container for the user management console for Administrators
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 20/04/2016
 * Time: 10:04 PM
 */

session_start(); // Required

$mID = $_GET['me'];
$uID = $_GET['id'];

// Verify if the user is an administrator
$sessionID = $_COOKIE['PHPSESSID'];

$name = 'User Management Console';

?>

<?php
require('tools/verifyAdmin.php');
$verification = new verifyAdmin();
$verified = $verification->verifyAdminAccess( $sessionID, $mID );

if ( !$verified ) {
    header('HTTP/1.1 403 Restricted Content');
    echo 'Access Denied';
    exit();
}
?>

<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../css/pdm/profMGMT.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="<?php echo dirname( $_SERVER['HTTP_REFERER'] );?>/../../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>

<body>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->
<?php
    // Main bulk of the site
    include(__DIR__.'/../headers/refreshclose.php');
?>
<iframe src='manageuser/main.php?me=<?php echo $mID; ?>&id=<?php echo $uID; ?>'> </iframe>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../js/materialize.js"></script>
</body>
</html>