<?php
/**
 * Changes the status of the user based on parameters given!
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 19/04/2016
 * Time: 2:17 PM
 */

$mID = $_GET['me'];

// Check if we have access first
require('../tools/verifyAdmin.php');
$verification = new verifyAdmin();
$verified = $verification->verifyAdminAccess( $_COOKIE['PHPSESSID'], $mID );

if (!$verified ) {
    header('HTTP/1.1 403 Restricted Content');
    echo ('Access denied');
    exit();
} else {
    require_once('../../core/connectDatabaseObj.php');
    session_start();


    $conObj = new dbConnect();
    $connection = $conObj->connect();

    $uID = $_GET['id'];
    $param = $_GET['p'];

// banned, probational, normal
    if ( $param !== '0' && $param !== '1' && $param !== '2' )
    {
        echo 'Invalid parameters given!';
        exit();
    }

    // Create status statement
    if ( $param === '0' )
    {
        $status = 'banned';
    }
    else if ( $param === '1' )
    {
        $status = 'probational';
    }
    else if ( $param === '2' )
    {
        $status = 'normal';
    }

    // Now we create the query and call it
    $stmt = "CALL changeStatus('$uID', '$status')";

    //echo '<br>';
    //echo $status;
    //echo $stmt;
    //echo '<br>';
    //print_r ( $connection );

    $result = mysqli_query( $connection, $stmt);

    if ( !$result )
    {   // If mysqli_query gives false!
        echo 'unknown error has occured';
        mysqli_close( $connection );
        exit();
    }

    mysqli_close( $connection );

// Go back to the previous page after this is done!
    header( 'Refresh: 5;' . $_SERVER['HTTP_REFERER']);
    include('../../loaders/dialogLoader.php');

    echo "<p class=center>Changing user's status to: <b>$status</b></p>";

    exit();
}
