<?php
/**
 * Deletes the user from the DB
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 20/04/2016
 * Time: 10:04 PM
 */

$uID = $_GET['id'];
$mID = $_GET['me'];

// Check if we have access first
require('../tools/verifyAdmin.php');
$verification = new verifyAdmin();
$verified = $verification->verifyAdminAccess( $_COOKIE['PHPSESSID'], $mID );

if (!$verified ) {
    header('HTTP/1.1 403 Restricted Content');
    echo ('Access denied');
    exit();
} else {
    require_once('../../core/connectDatabaseObj.php');
    session_start();

    $conObj = new dbConnect();
    $connection = $conObj->connect();

    $stmt = "CALL delUserSecure('$uID');";
    $result = $connection->query($stmt);

// Go back to the previous page after this is done!
    header( 'Refresh: 5;' . $_SERVER['HTTP_REFERER']);
    include('../../loaders/dialogLoader.php');

    echo "<p class=center>Marking user with userID: <b>$uID</b> for deletion</p>";

    exit();
}
