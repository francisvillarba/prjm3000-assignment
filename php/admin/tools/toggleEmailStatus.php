<?php
/**
 * Runs a function from the DB in order to toggle the Email status of the user
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 19/04/2016
 * Time: 1:48 PM
 */

$mID = $_GET['me'];

// Check if we have access first
require('../tools/verifyAdmin.php');
$verification = new verifyAdmin();
$verified = $verification->verifyAdminAccess( $_COOKIE['PHPSESSID'], $mID );

if (!$verified ) {
    header('HTTP/1.1 403 Restricted Content');
    echo ('Access denied');
    exit();
} else {
    //echo ("Access granted");
    require_once('../../core/connectDatabaseObj.php');
    session_start();

    $conObj = new dbConnect();
    $connection = $conObj->connect();

    $uID = $_GET['id'];

    $stmt = "CALL emailVerified('$uID');";
    $result = $connection->query($stmt);
    //echo 'Email Verified!';

    //No longer toggle required
    /*if ( $toggle !== '1' && $toggle !== '0' ) {
        echo 'Invalid arguments given to function toggleEmailStatus' ;
        return false;
    }

    if ( $toggle === '1' ) {
        $stmt = "CALL emailVerified('$uID');";
        $result = $connection->query($stmt);
        echo '<br>';
    } else if ( $toggle === '0' ) {
    }*/

// Go back to the previous page after this is done!
    header( 'Refresh: 5;' . $_SERVER['HTTP_REFERER']);
    include('../../loaders/dialogLoader.php');

    echo "<p class=center>Toggling user's e-mail verification status to: <b>Verified</b></p>";

    exit();
}

