<?php
/**
 * Gets the submission information based on sID for the administrators
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 27/04/2016
 * Time: 6:11 PM
 *
 */

class getSubmission
{
    public static $connection;

    /**
     * Create a connection to the DB
     * Remember constructor needs double _'s to work
     */
    public function __construct()
    {
        require_once(__DIR__ . '/../../core/connectDatabaseObj.php');
        $dbConnection = new dbConnect();
        self::$connection = $dbConnection->connect();
        // print_r (self::$connection = $dbConnection->connect() );
    }

    /**
     * 
     */
}