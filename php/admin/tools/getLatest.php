<?php
/**
 * Gets the latest information by a user for administrators!
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 27/04/2016
 * Time: 6:11 PM
 *
 */

require_once('../../core/tools/stringParser.php');

class getLatest
{

/**  General stuff *****************************************************************************************************/

    public static $connection;

    /**
     * Create a connection to the DB
     * Remember constructor needs double _'s to work
     */
    public function __construct()
    {
        require_once(__DIR__ . '/../../core/connectDatabaseObj.php');
        $dbConnection = new dbConnect();
        self::$connection = $dbConnection->connect();
        // print_r (self::$connection = $dbConnection->connect() );
    }

    /**
     * Runs a query on the DB and returns the result
     *
     * @param $stmt String The statement to run
     * @return mixed The result of the mysqli::query() function
     */
    private function query( $stmt )
    {
        return mysqli_query( self::$connection, $stmt );
    }

/**  Submissions ******************************************************************************************************/

    /**
     * Gets the latest submission ID based on the user's information
     * @param $uID string the user's ID to poll the db for
     * @return string the latest post made by the user
     */
    private function getSID( $uID )
    {
        // Get the submission ID
        $stmt = "SELECT getLastSubmission( '$uID' )";

        $status = $this->query( $stmt );

        // Convert into an array
        $rows = array();
        while ($row = $status -> fetch_assoc())
        {
            $rows[] = $row;
        }

        $test = $rows[0]; // Get the singular value
//        $test2 = $test["getStatus('$mID')"];

        $test2 = $test["getLastSubmission( '$uID' )"];

//        // Debug -- print array
//        echo'debug 1';
//        echo '<pre>';
//        print_r ( "rows: " . $rows );
//        echo '</pre>';
//
//        // Debug -- Attempt to print the singular values
//        echo'debug 2';
//        echo '<br>';
//        print_r ( $test );
//        echo '<br>';
//        print_r ( $test2 );
//        echo '<br>';

        return $test2;
    }


    /**
     * Gets the submission information
     * @param $uID string the submission ID
     * @return string result from the DB
     */
    private function getSInfo( $sID )
    {
        // Get the submission ID
        $stmt = "SELECT getSubmission( '$sID' )";
        $status = $this->query( $stmt );
        // Convert into an array
        $rows = array();
        while ($row = $status -> fetch_assoc())
        {
            $rows[] = $row;
        }
        $test = $rows[0]; // Get the singular value
        $test2 = $test["getSubmission( '$sID' )"];
        return $test2;
    }


    /**
     * Gets the latest submission information based on the user's ID
     * @param $uID string the user's ID to poll the db for
     * @return array representation of the submission (does not include content)
     */
    public function getSubmissionInfo( $uID )
    {
        // Get the submission information
        $sID = self::getSID($uID);
        // Get the submission itself
        $rawResult = self::getSInfo( $sID );

        // Parse the result
        $parser = new parser( $rawResult );
        $result = $parser->getArray(';');

        // Return the submission information
//        print_r ($result);
        return $result;
    }

    /**
     * Gets the submission content
     * @param $sID string the submissionID to get the content for
     * @return string the contents of the submission
     */
    public function getSubmissionContent( $sID )
    {
        // TODO - this implementation is a stub
        // This should be calling a function from getSubmission tool :P
    }

/**  Comments *********************************************************************************************************/


    /**
     * Gets the latest comment made by a user
     * @param $uID string the user ID
     * @return string the latest commentID as a string
     */
    private function getLatestCommentID( $uID )
    {
        // Get the submission ID
        $stmt = "SELECT getLastComment( '$uID' )";
        $status = $this->query( $stmt );
        // Convert into an array
        $rows = array();
        while ($row = $status -> fetch_assoc())
        {
            $rows[] = $row;
        }
        $test = $rows[0]; // Get the singular value
        $test2 = $test["getLastComment( '$uID' )"];
        return $test2;
    }

    /**
     * Gets the latest comment content made by a user
     * @param $cID string the user ID
     * @return string the latest comment the user has made as a string!
     */
    private function getCommentContent( $cID )
    {
        // Get the submission ID
        $stmt = "SELECT getComment( '$cID' )";
        $status = $this->query( $stmt );
        // Convert into an array
        $rows = array();
        while ($row = $status -> fetch_assoc())
        {
            $rows[] = $row;
        }
        $test = $rows[0]; // Get the singular value
        $test2 = $test["getComment( '$cID' )"];
        return $test2;
    }

    /**
     * Gets the latest comment made by a user
     * @param $uID string the user ID
     * @return array the latest comment the user has made as a string!
     */
    public function getLatestComment( $uID )
    {
        $cID = self::getLatestCommentID( $uID );
        $result = self::getCommentContent( $cID );
        // Parse the result
        $parser = new parser( $result );
        $finalResult = $parser->getArray(';');
        return $finalResult;
    }
}