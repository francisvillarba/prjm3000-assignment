<?php
/**
 * The purpose of this module is to open a profile manager for a given user -- and will give access to the user IFF
 * they are an administrator with a valid session ID (in addition to checking if they have logged in at all :P
 *
 * Created in phpStorm
 * User: Francis Villarba
 * Date: 7/04/2016
 * Time: 12:33 PM
 */
/*session_start();

require(__DIR__.'/../../core/tools/verifyLoggedIn.php');        // Needed to check if they are logged in
require(__DIR__.'/../../core/tools/getUserStatus.php');         // Needed to get the status of the user logged in


// Check login
$checkLogin = new session();
$isLoggedIn = $checkLogin->verifySession( $sessionID );

// Check status of the logged in user
$checkStatus = new userStatus();
$status = $checkStatus->getStatus( $mID );

//echo '<br>';
//print_r ($isLoggedIn);
//echo '<br>';
//print_r ($status);
//echo '<br>';

if ( $isLoggedIn ) {
    if ( $status === 'admin' ) {
        return $verified = true;
    }
    else
    {
        return $verified = false;
    }
}*/

require(__DIR__.'/../../core/tools/verifyLoggedIn.php');        // Needed to check if they are logged in
require(__DIR__.'/../../core/tools/getUserStatus.php');         // Needed to get the status of the user logged in


class verifyAdmin
{
    public static $connection;

    /**
     * Create a connection to the DB
     * Remember constructor needs double _'s to work
     */
    public function __construct()
    {
        require_once(__DIR__ . '/../../core/connectDatabaseObj.php');
        $dbConnection = new dbConnect();
        self::$connection = $dbConnection->connect();
        // print_r (self::$connection = $dbConnection->connect() );
    }

//    public function verifyAdminAccess( $session, $mID ) {
//        $checkLogin = new session();
//        $isLoggedIn = $checkLogin->verifySession( $session );
////        echo $isLoggedIn;
//
//        if ( $isLoggedIn )
//        {
//            // Do some more checks
//            // Check status of the logged in user
//            $checkStatus = new userStatus();
//            $status = $checkStatus->getStatus( $mID );
////            echo ('level1');
//
//            if ( $status === 'admin' ) {
////                echo ('level2');
//                return true;
//            }
//            else
//            {
////                echo ('level3');
//                return false;
//            }
//
//        } else {
////            echo ('level0');
//            return false;
//        }
//    }

    public function verifyAdminAccess( $session, $mID ) {
        // Do some more checks
        // Check status of the logged in user
        $checkStatus = new userStatus();
        $status = $checkStatus->getStatus( $mID );
//            echo ('level1');

        if ( $status === 'admin' ) {
//                echo ('level2');
            return true;
        }
        else
        {
//                echo ('level3');
            return false;
        }
    }
}