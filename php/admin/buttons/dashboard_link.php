<?php
/**
 * Created by PhpStorm.
 * User: francis
 * Date: 18/05/2016
 * Time: 11:56 PM
 */

// Verify if the user is an administrator
$sessionID = $_COOKIE['PHPSESSID'];

// Get the user ID
include_once('../core/connectDatabase.php');

// Get their User ID
$uidQueryRes = mysqli_fetch_row( mysqli_query( $connection, "SELECT getIDFromSession('$sessionID');") );
//echo $uidQueryRes[0];
$userID = $uidQueryRes[0];

require('../admin/tools/verifyAdmin.php');
$verification = new verifyAdmin();
$verified = $verification->verifyAdminAccess( $sessionID, $userID );

if ( !$verified )
{
    // header('HTTP/1.1 403 Restricted Content');
//    echo 'FAILURE TO CHECK ADMIN RIGHTS';
//    echo '<br';
//    exit();
}
else
{
    echo
    '
    <i id="mm-admin-icon" onclick="slideToAdmin()" class="large material-icons waves-effect waves-light" style="text-shadow:2px 2px 4px #333333;">contacts</i>
    <p id="mm-admin-icon">Administration</p>
    ';
}
?>