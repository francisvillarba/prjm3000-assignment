<?php
/**
 * User Interface for Modifying the Account information such as Email
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 20/04/2016
 * Time: 9:15 PM
 */

session_start();

$uID = $_GET['me'];

if (!$verified )
{
    header('HTTP/1.1 403 Restricted Content');
    echo ('Access denied');
    exit();
}

?>
<!-- Style Stuff -->
<link type="text/css" rel="stylesheet" href="../../../css/materialize-fonts.css"/>
<link type="text/css" rel="stylesheet" href="../../../css/materialize.min.css" media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="../../../css/pdm/profMGMT.css"/>

<h5 class="center">Modify Account Information</h5>
<hr>
<div class="row"> <!-- First Name -->
    <div class="input-field col s6">
        <i class="material-icons prefix active">face</i>
        <input placeholder="Original Name" disabled id="original-firstName" type="text" class="validate">
        <label for="original-firstName" data-error="invalid data given" data-success="" class="active"></label>
    </div>
    <div class="input-field col s6">
        <input id="firstName" type="text" class="validate">
        <label for="firstName" data-error="invalid data given" data-success="" class="active">New First Name</label>
    </div>
</div>
<div class="modal-header">
    <div class="row"> <!-- Last Name -->
        <div class="input-field col s6">
            <i class="material-icons prefix active">supervisor_account</i>
            <input placeholder="Original Last Name" disabled id="original-LastName" type="text" class="validate">
            <label for="original-LastName" data-error="invalid data given" data-success="" class="active"></label>
        </div>
        <div class="input-field col s6">
            <input id="lastName" type="text" class="validate">
            <label for="lastName" data-error="invalid data given" data-success="" class="active">New Last Name</label>
        </div>
    </div>
    <div class="row"> <!-- E-mail address -->
        <div class="input-field col s6">
            <i class="material-icons prefix active">mail</i>
            <input placeholder="Original E-mail address" disabled id="original-Email" type="text" class="validate">
            <label for="original-Email" data-error="invalid data given" data-success="" class="active"></label>
        </div>
        <div class="input-field col s6">
            <input id="email" type="text" class="validate">
            <label for="email" data-error="invalid data given" data-success="" class="active">New E-mail Address</label>
        </div>
    </div>
</div>
<div class="modal-footer right-align">
    <a href="#confirmationCancel" class="modal-action modal-trigger modal-close waves-effect btn-flat">Cancel</a>
    <a href="#confirmationSave" class="modal-action modal-trigger waves-effect waves-light btn-flat teal white-text">Save</a>
</div>

<!-- Confirmation Dialogs Save-->
<div id="confirmationSave" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>Modify user data?</h4>
        <p>Are you sure you want to save changes?</p>
    </div>
    <div class="modal-footer">
        <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat teal white-text">Yes</a>
        <a class="modal-action modal-close waves-effect btn-flat">Cancel</a>
    </div>
</div>

<!-- Confirmation Dialogs Cancel-->
<div id="confirmationCancel" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>Cancel Changes?</h4>
        <p>Are you sure you want to cancel changes?</p>
    </div>
    <div class="modal-footer">
        <a href="main.php?me=<?php echo $mID; ?>&id=<?php echo $uID; ?>" class=" modal-action modal-close waves-effect waves-green btn-flat teal white-text">Yes</a>
        <a class="modal-action modal-close waves-effect btn-flat">No</a>
    </div>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="../../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../../../js/materialize.js"></script>
<script type="text/javascript" src="../../../js/init_manageUser.js"></script>