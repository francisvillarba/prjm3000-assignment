<?php
/**
 * The main content page of the user management console
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 8/04/2016
 * Time: 2:37 PM
 */


require('../../core/tools/getUserInfo.php');
require('../../core/tools/stringParser.php');
require('../tools/getLatest.php');

session_start();

$mID = $_GET['me'];
$uID = $_GET['id'];

include('../../loaders/modalLoader.php');

// Check if we have access first
require('../tools/verifyAdmin.php');
$verification = new verifyAdmin();
$verified = $verification->verifyAdminAccess( $_COOKIE['PHPSESSID'], $mID );

if ( !$verified ) {
    header('HTTP/1.1 403 Restricted Content');
    echo 'Access Denied';
    exit();
}

// Get the user information
$userInfo = new userInfo();
$result = $userInfo->getUserInfo( $uID );
$test = $result[0];
$test2 = $test["getUser('$uID')"];
$parser = new parser( $test2 );
$array = $parser->getArray( ';' );

// Check the latest posts and such made by the user
$latestInfo = new getLatest();
$latestPost = $latestInfo->getSubmissionInfo( $uID );
$latestComment = $latestInfo->getLatestComment( $uID );

//print_r ( $latestPost );
//echo '<br>';
//print_r ( $latestComment );

?>
<html>
<meta name="keywords" content="iForget, PDM, PRJM3000, Curtin University, Project, Suggestions, Suggestion-Box">
<meta name="description" content="Suggestion Box - A project providing students and educators a new way to share and communicate ideas, suggestions and concepts">

<!-- Style Stuff -->
<link type="text/css" rel="stylesheet" href="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../css/materialize-fonts.css"/>
<link type="text/css" rel="stylesheet" href="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../css/materialize.min.css" media="screen,projection"/>
<link type="text/css" rel="stylesheet" href="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../css/loading.css"/>
<link type="text/css" rel="stylesheet" href="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../css/pdm/profMGMT.css"/>


<!-- Responsive Design + Compatibility -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<body>
<!-- What the admin will see -->
<div class="row">
    <div class="col s12">
        <ul class="tabs center fixed">
            <li class="tab col s3"><a class="active teal-text waves-effect" href="#tabGeneral">General</a></li>
            <li class="tab col s3"><a class="teal-text waves-effect" href="#tabAccount">Account</a></li>
            <li class="tab col s3"><a class="teal-text waves-effect" href="#tabModeration">Moderation</a></li>
        </ul>
    </div>
    <div id="tabGeneral" class="col s12"> <!-- General Tab -->
        <?php
        if ( $array[6] === 'deleted' ) {
            echo "<h5 class='center'><b>The user was marked for deletion!</b></h5>";
            echo "<p class='center text-black'>Please note! This information may not be available in the future once deletion has been processed!</p>";
        }
        ?>
        <div class="avatar"><p id="placeHolderImage" class="avatar"><i class="material-icons huge black-text avatar">person</i></p></div>
        <div class="nextToImage"> <!-- Anyting next to the image goes here -->
            <p class="teal-text">
                First Name: &emsp;
                <span id="firstNameField" class="nextToImage black-text">
                    <?php echo $array[0]; ?>
                </span>
            </p>
            <p class="teal-text">Last Name: &emsp;
                <span id="lastNameField" class="nextToImage black-text">
                    <?php echo $array[1]; ?>
                </span>
            </p>
            <p class="teal-text">Email: &emsp;&emsp;&emsp;&nbsp;&nbsp;
                <span id="emailField" class="nextToImage black-text">
                    <?php echo $array[2]; ?>
                </span>
            </p>
            <p class="teal-text">Ranking: &emsp;&emsp;&nbsp;&nbsp;
                <span id="rankField" class="nextToImage black-text">
                    <?php echo $array[4]; ?>
                </span>
            </p>
            <p class="teal-text">Join Date: &emsp;&nbsp;&nbsp;&nbsp;
                <span id="joinField" class="nextToImage black-text">
                    <?php echo $array[3]; ?>
                </span>
            </p>
        </div>
        <div>
            <br><br><br><br><br><br>
        </div>
        <footer class="page-footer white">
            <!--            <div class="container">-->
            <!--                <div class="row right-align">-->
            <!--                    <a href="modifyinfo.php?me=<?php //echo $uID;?><!--" class="waves-effect waves-light btn teal modal-trigger">Modify</a>-->
            <!--                </div>-->
            <!--            </div>-->
        </footer>
    </div>
</div>

<!-- Account Information Tab -->
<div id="tabAccount" class="col s12">
    <p class="teal-text">
        &emsp;&nbsp;&nbsp;&nbsp; Account ID: &emsp;&emsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <span id="accID" class="black-text">
            <?php echo $uID; ?>
        </span>
    </p>
    <p class="teal-text">
        &emsp;&nbsp;&nbsp;&nbsp; Join Date: &emsp;&nbsp;&nbsp;&nbsp;&emsp;&emsp;
        <span id="joinField2" class="black-text">
            <?php echo $array[3]; ?>
        </span>
    </p>
    <p class="teal-text">&emsp;&nbsp;&nbsp;&nbsp; Account Status: &emsp;&nbsp;
        <span id="accStatus" class="black-text">
            <?php echo $array[6]; ?>
        </span>
    </p>
    <p class="teal-text">&emsp;&nbsp;&nbsp;&nbsp; E-mail verified: &emsp;&nbsp;&nbsp;&nbsp;
        <span id="emailStatus" class="black-text">
            <?php
            if ( $array[5] === '1' ) {
                echo 'Yes';
            } else {
                echo 'No';
            }
            ?>
        </span>
    </p>
    <ul class="collapsible z-depth-0" data-collapsible="accordion">
        <li>
            <div class="collapsible-header"><i class="material-icons">bubble_chart</i>Latest Suggestion</div>
            <div class="collapsible-body center-align">
                <p class="black-text">
                    <b class="teal-text">Title: </b><?php echo $latestPost[1]; ?>
                    <b class="teal-text"> Category: </b><?php echo $latestPost[2]; ?>
                    <b class="teal-text"> When: </b><?php echo $latestPost[3]; ?></p>
                <!-- TODO implement the loading of comment content! -->
                <p class="black-text">
                    TODO -- Not implemented loading of comment content yet!
                </p>
            </div>
        </li>
        <li>
            <div class="collapsible-header"><i class="material-icons">chat_bubble</i>Latest Comment</div>
            <div class="collapsible-body center-align">
                <p class="black-text">
                    <b class="teal-text"> SubmissionID: </b><?php echo $latestComment[2]; ?>
                    <b class="teal-text"> Parent Comment: </b><?php echo $latestComment[1]; ?>
                <p class="black-text">
                    <?php echo $latestComment[3]; ?> </p>
                </p>
            </div>
        </li>
    </ul>
</div>

<!-- Account Moderation Tab -->
<div id="tabModeration" class="col s12">
    <div class="center">
        <br>
        <p class="black-text">
            <b>E-mail status: </b>
            <?php
            if ( $array[5] === '1' ) {
                echo 'Yes';
            } else {
                echo 'No';
            }
            ?>
        </p>
        <p class="black-text">Modify E-mail status</p>
        <a href='../tools/toggleEmailStatus.php?me=<?php echo $mID ?>&id=<?php echo $uID ?>' class="waves-effect waves-light btn">
            <i class="material-icons left">mail</i>Make Verified</a>
    </div>
    <div class="center">
        <br>
        <p class="black-text"><b>User Status:</b> <?php echo $array[6]; ?> </p>
        <p class="black-text">Modify User Status</p>
        <a href='../tools/changeStatus.php?me=<?php echo $mID ?>&id=<?php echo $uID ?>&p=0' class="waves-effect waves-light btn materialize-red">
            <i class="material-icons left">report</i>Ban</a>
        <a href='../tools/changeStatus.php?me=<?php echo $mID ?>&id=<?php echo $uID ?>&p=1' class="waves-effect waves-light btn orange">
            <i class="material-icons left">find_replace</i>Probational</a>
        <a href='../tools/changeStatus.php?me=<?php echo $mID ?>&id=<?php echo $uID ?>&p=2' class="waves-effect waves-light btn teal">
            <i class="material-icons left">account_circle</i>Normal</a>
    </div>
    <div class="center">
        <br>
        <p class="black-text"><b>Account Modification</b></p>
        <a href="#confirmationDelete" class="modal-action modal-trigger waves-effect waves-light btn materialize-red"><i class="material-icons left">warning</i>Delete Account</a>
    </div>
</div>

<!-- Confirmation Delete-->
<div id="confirmationDelete" class="modal bottom-sheet">
    <div class="modal-content">
        <h4>Delete Account</h4>
        <p>Are you sure you want to delete account?</p>
    </div>
    <div class="modal-footer">
        <a href="../tools/deleteUser.php?me=<?php echo $mID ?>&id=<?php echo $uID ?>" class=" modal-action modal-close waves-effect waves-green btn-flat teal white-text">Yes</a>
        <a class="modal-action modal-close waves-effect btn-flat">No</a>
    </div>
</div>

<!-- It is generally best practise to load javascript at the end to decrease load times! -->
<script type="text/javascript" src="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../js/materialize.js"></script>
<script type="text/javascript" src="<?php echo dirname( $_SERVER['REQUEST_URI'] );?>/../../../js/init_manageUser.js"></script>
</body>
</html>