<?php
/**
 * The landing page for the project -- where the user will be redirected to if they have not logged in
 * and can redirect the user if they don't have an account to a registration form :)
 *
 * Created by PhpStorm.
 * User: Francis Villarba
 * Date: 1/05/2016
 * Time: 6:46 PM
 */

require('core/tools/verifyLoggedIn.php');        // Needed to check if they are logged in

// Check login
$checkLogin = new session();
$isLoggedIn = $checkLogin->verifySession( $_COOKIE[PHPSESSID] );
$result = !$isLoggedIn; // Use below if we always want to see the landing page
// $result = false;

$host = $_SERVER[ 'HTTP_HOST' ];
//echo $_COOKIE[PHPSESSID];
//echo '<br>';
//echo $result;
if ( $result ) {
    // Go to the index page
    header('HTTP/1.1 100 Continue');
    header('Location: ' . 'index.php');
    echo "<p class='center'>Welcome back to Suggestion Box!</p>";
    echo "<p class='center'>Redirecting you in 3 seconds\n</p>";
    echo "<p class='center'>If you have not been redirected, click <a href='https://$host/php/index.php'>here</a></p>";
    exit();
}

include( 'loaders/loader.php');
?>

<!DOCTYPE html>
<!-- For Modernizr to check browser support -->
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->

<head>
    <!-- Information for the Site -->
    <meta charset="UTF-8">
    <title>Suggestion Box</title>

    <meta name="keywords" content="iForget, PDM, PRJM3000, Curtin University, Project, Suggestions, Suggestion-Box">
    <meta name="description"
          content="Suggestion-Box - A project providing students and educators a new way to share and communicate ideas, suggestions and concepts">

    <!-- Style Stuff -->
    <link type="text/css" rel="stylesheet" href="../css/materialize-fonts.css"/>
    <link type="text/css" rel="stylesheet" href="../css/materialize.min.css" media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="../css/main.css"/>
    <link type="text/css" rel="stylesheet" href="../css/pdm/landing.css"/>

    <!-- Responsive Design + Compatibility -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="apple-touch-icon" href="../apple-touch-icon.png">
    <script src="../js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
</head>
<body>

<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
    your browser</a> to improve your experience.</p>
<![endif]-->

<div class="container valign-wrapper">
    <div id="Prompt" class="card-panel hoverable valign"> <!-- Main body div -->
        <div class="row">
            <h4 class="center">Suggestion Box</h4>
            <p class="center">Please login to continue</p>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix green-text text-darken-3">email</i>
                <input id="username" type="email" class="validate">
                <label for="username" data-error="Invalid input" data-success="">Email Address</label>
            </div>
        </div>
        <div class="row">
            <div class="input-field col s12">
                <i class="material-icons prefix green-text text-darken-3">vpn_key</i>
                <input id="password" type="password" class="validate">
                <label for="password">Password</label>
            </div>
        </div>
        <div class="row center">
            <a class="waves-effect waves-light green darken-3 btn-flat white-text center modal-trigger" href="#wait" onclick="loginMain()">Login</a>
        </div>
        <br>
        <div class="row">
            <!-- Medium and up displays -->
            <p class="center show-on-medium-and-up hide-on-small-and-down">Don't have an account?  <a href="tos.php">Click here to register!</a></p>
            <p class="center show-on-medium-and-up hide-on-small-and-down">Want to learn more about Suggestion Box?  <a href="about.php">Click here to get more info!</a></p>

            <!-- Anything smaller -->
            <p class="center hide-on-med-and-up show-on-small">Don't have an account?</p>
            <p class="center hide-on-med-and-up show-on-small"><a href="tos.php">Click here to register!</a></p>
            <p class="center hide-on-med-and-up show-on-small">Want to learn more about Suggestion Box?</p>
            <p class="center hide-on-med-and-up show-on-small"><a href="about.php">Click here to get more info!</a></p>
        </div>
    </div>
</div>

<div id="wait" class="modal">
    <div class="modal-content">
        <h5 class="center">Please wait</h5>
        <p class="center">Communicating with Server</p>
        <div class="progress">
            <div class="indeterminate"></div>
        </div>
    </div>
</div>

<!-- Scripts -->
<script type="text/javascript" src="../js/vendor/jquery-1.11.2.js"></script>
<script type="text/javascript" src="../js/materialize.js"></script>
<script type="text/javascript" src="../js/pdm/verify.js"></script>

<!-- Login Specific Code -->
<script type="text/javascript" src="../js/pdm/clearLog.js"></script>
<script type="text/javascript" src="../js/pdm/login.js"></script>
<script type="text/javascript" src="../js/pdm/landing.js"></script>
</body>
</html>